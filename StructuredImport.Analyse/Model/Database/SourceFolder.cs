﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace StructuredImport.Analyse.Model.Database
{
    public class SourceFolder
    {
        public  int SourceFolderId { get; set; }
        public string FolderName { get; set; }
        public string RelativePath { get; set; }    
    }
}
