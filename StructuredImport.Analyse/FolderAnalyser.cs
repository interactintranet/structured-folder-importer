﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using StructuredImport.Analyse.Model.Database;
using StructuredImport.Core.Model;

namespace StructuredImport.Analyse
{
    public class FolderAnalyser
    {
        private readonly AppConfig _testConfig;
        protected SqlConnection _dbMetadata;

        public FolderAnalyser(AppConfig testConfig)
        {
            _testConfig = testConfig;
            _dbMetadata = new SqlConnection(_testConfig.MetadataDatabaseConnectionString);
        }

        public async Task ClearTable()
        {
            await _dbMetadata.ExecuteAsync("DELETE  from Core.SourceFolder");
        }

        private string _root;
        /// <summary>
        /// Starts at folderPath and finds sibling folders and descendent folders and adds them to the SourceFolder table.
        /// You can call ClearTable before this to rebuild the set from scratch, otherwise just new folders are added.
        /// </summary>
        /// <param name="folderPath"></param>
        /// <returns></returns>
        public async Task LoadFolderStructureFrom(string folderPath)
        {
            _root = folderPath;
            await LoadFolderStructureAt(folderPath);
            _dbMetadata.Execute("UPDATE Core.RootFolder SET RootFolderPath=@Root", new
            {
                Root = _root
            }); //Root Folder is a single row table
            _dbMetadata.Execute("[Core].[PopulateTargetLocationTable]");
        }
        internal async Task LoadFolderStructureAt(string folderPath, int? parentId = null)
        {
            var fls = Directory.EnumerateDirectories(folderPath).ToList();
            foreach (string folder in fls)
            {
                var relPart = folder.Replace(_root, string.Empty);
                var dir = new DirectoryInfo(folder);
                var folderName = dir.Name;
                var fileCount = dir.EnumerateFiles().ToList().Count;
                var folderInfo = await _dbMetadata.QueryFirstOrDefaultAsync<SourceFolder>("Core.SourceFolderAdd @Path, @FolderName, @ParentId, @FileCount", new
                {
                    @Path = relPart, FolderName = folderName, ParentId = parentId, fileCount
                });
                if (fileCount > 0)
                {
                    await LoadFileMakeupAt(folder, folderInfo);
                }

                await LoadFolderStructureAt(folder, folderInfo.SourceFolderId);

                Console.WriteLine($"Processed folder {folderPath}");
            }
        }

        private async Task LoadFileMakeupAt(string folder, SourceFolder folderInfo)
        {
            var dir = new DirectoryInfo(folder);
            foreach (var fileInfo in dir.EnumerateFiles())
            {
                await _dbMetadata.ExecuteAsync("Core.SourceFileAdd @FileName, @FileSize, @FolderId, @Extension", new
                {
                    FileName = fileInfo.Name,FileSize= fileInfo.Length, FolderId = folderInfo.SourceFolderId, fileInfo.Extension
                });
            }
        }
    }
}
