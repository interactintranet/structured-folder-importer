﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using StructuredImport.Core.Support;

namespace StructuredImport.Test
{
    [TestFixture]
    class DbLoggerTest
    {
        DbLogger _db;
        [OneTimeSetUp]
        public void Prep()
        {
            var config = TestHelper.GetTestConfig();

             _db = new DbLogger(config);
        }


        [Test]
        public async Task TestActionLogFolder()
        {
            _db.ActionLogFolder(5,"a",1,1, "");
        }

        [Test]
        public void TestActionLogProcessFilesInFolder()
        {
            _db.ActionLogProcessFilesInFolder(22,"", 4 );
        }

        [Test]
        public void TestActionPageCreationFailed()
        {
            _db.ActionPageCreationFailed(23,@"\UU", "magic","Fail simulated");
        }

        [Test]
        public void TestActionFileFoundInTarget()
        {
            _db.ActionFileFoundInTarget("moues", "lla",22, @"d:\x\y");
        }

        [Test]
        public void TestScalarsAreWrittenToAuxiliaryTable()
        {
            _db.ActionLogProcessFilesInFolderCompleted(33, 1, 22, 4, 55, "Test", 99);
        }
    }
}
