﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using StructuredImport.Analyse;

namespace StructuredImport.Test.AnalyserTests
{
    [TestFixture]
    public class AnalyserTests
    {
        [Test]
        //[Ignore("Destructive")]
        public async Task ThatFolderTableIsFiller()
        {

            var testConfig = TestHelper.GetTestConfig();
            var folderAnalyser = new FolderAnalyser(testConfig);
            //await folderAnalyser.ClearTable();
            await folderAnalyser.LoadFolderStructureFrom(@"C:\develop\StructImporter\SampleStruct1");


        }
    }
}
