﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StructuredImport.Core.Model;

namespace StructuredImport.Test
{
    class TestHelper
    {
        public static AppConfig GetTestConfig()
        {
            return new AppConfig
            {
                InteractApiUrl = "https://eu-lb-api-01.interactgo.com/",
                InteractUserName = "richard.garner@interact-intranet.com",
                InteractPassword = "Airport5",
                InteractTenantId = "73779690-30e4-41b3-971e-68699ad30a66",
                MetadataDatabaseConnectionString = "Server=.;Database=StructuredImport_Test;Trusted_Connection=true",
                LicencePath = @"C:\Git\StructuredFolderImporter\StructuredImport\Aspose.Total.lic"
            };
        }
        public static AppConfig GetTestConfig2()
        {
            return new AppConfig
            {
                InteractApiUrl = "https://eu-lb-api-01.interactgo.com/",
                InteractUserName = "RKG",
                InteractPassword = "abc123XYZ!",
                InteractTenantId = "df235878-7c99-4f18-9568-25888fef818a",
                MetadataDatabaseConnectionString = "Server=.;Database=StructuredImport_Test;Trusted_Connection=true",
                LicencePath = @"C:\Git\StructuredFolderImporter\StructuredImport\Aspose.Total.lic"
            };
        }
    }
}
