﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using StructuredImport.Core.App;
using StructuredImport.Core.Model;
using StructuredImport.Core.Model.AppPage;
using StructuredImport.Core.Model.Database;

namespace StructuredImport.Test.ProcessorTests
{
    [TestFixture]
    class DbProcessorTests
    {
        [Test, Ignore("evolutionary test")]
        public async Task ThatMarkPageAsCreatedWorks()
        {

            var config = TestHelper.GetTestConfig();
            var dbProcessor = new ImportProcessorDatabaseDriven(config);
            var contentItem = new FileContentItem
            {
                FullPath = @"c:\test\test\test",
                FileName = "Auotmated test", Size = 1
            };
            var folderInfo = new FolderInfo
            {
                FolderName = "test", FolderId = -1, SectionId = 1, CategoryId = 99
            };
            var pageCreateResult = new PageCreateResult
            {
                PageId = 9, PageWasCreated = true, ProcessTime = new TimeSpan(25000)
            };
            await dbProcessor.MarkPageAsCreated(contentItem, folderInfo, pageCreateResult);
        }
    }
}
