﻿using System.Threading.Tasks;
using NUnit.Framework;
using StructuredImport.Core.Support;

namespace StructuredImport.Test.MiscTest
{
    /// <summary>
    /// CAUTION: Operations in this test are potentially destructive
    /// </summary>
    [TestFixture]
    
    class PageClearerTests
    {
        [Test]
        public async Task ThatPageClearerRuns()
        {
            //WARNING: this operation is potentially destructive
            var config = TestHelper.GetTestConfig();
            var pageDeleter = new DeleteProcessor(config);

            await pageDeleter.ProcessDeleteQueue(true);
        }
    }
}
