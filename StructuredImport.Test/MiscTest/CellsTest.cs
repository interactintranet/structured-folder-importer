﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using StructuredImport.Core.Support;

namespace StructuredImport.Test.MiscTest
{
    [TestFixture]
    internal class CellsTest
    {
        [Test]
        public async Task OpenExcelFile()
        {
            var config = TestHelper.GetTestConfig();
            var sgOutputDocsDoc = new ShareGateOutputDocumentSpreadsheet(config, @"C:\temp-rga\SharegateEval\Q\Q\Q.xlsx");
            sgOutputDocsDoc.ParseDoc();

            Assert.That(sgOutputDocsDoc.DocumentList.Count, Is.Not.Zero);

        }
    }
}
