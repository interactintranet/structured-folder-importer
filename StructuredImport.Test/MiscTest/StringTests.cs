﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace StructuredImport.Test.MiscTest
{
    [TestFixture]
    class StringTests
    {
       
        [Test]
        public void ThatSubstringOnAShortStringThrowsAnException()
        {
            var a = "hello";
            //Assert.That(()=> a.Substring(0, 500), Throws.ArgumentException);
            Assert.Throws<ArgumentOutOfRangeException>(() => a.Substring(0, 500));
        }
        

        [TestCase("hello", 4)]
        [TestCase("hell", 4)]
        [TestCase("ell", 3)]
        [TestCase("lo", 2)]
        public void TestTrim(string a, int c)
        {
            var b = a.PadRight(4).Substring(0, 4).Trim();
            Assert.That(b.Length, Is.EqualTo(c));
        }
    }
}
