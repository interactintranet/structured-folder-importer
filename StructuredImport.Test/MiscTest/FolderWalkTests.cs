﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using StructuredImport.Core.App;

namespace StructuredImport.Test.MiscTest
{
    [TestFixture]
    class FolderWalkTests
    {
        [Test]
        public async Task Walk1()
        {
            var startAt = @"C:\develop\StructImporter\SampleStruct1";
            var fm = new FolderManager(startAt, false);
            Assert.AreEqual(fm.CurrentFolder, startAt);
        }

        [Test]
        public async Task ThatEmptyFolderLoadsNoItems()
        {
            var startAt = @"C:\develop\StructImporter\SampleStruct1";
            var fc = new FolderContentManager(startAt);
            var files = fc.FileList;
            Assert.That(fc.FileList.Count, Is.EqualTo(0));
        }

        [Test]
        public async Task ThatFolderLoadsAllItems()
        {
            var startAt = @"C:\develop\StructImporter\SampleStruct1\FolderA";
            var fc = new FolderContentManager(startAt);
            var files = fc.FileList;
            Assert.That(fc.FileList.Count, Is.GreaterThan(0));
        }

        [Test]
        public async Task ThatFolderWithMetadataFilesDetectsThem()
        {
            var startAt = @"C:\develop\StructImporter\SampleStruct1\FolderA";
            var fc = new FolderContentManager(startAt);
            var files = fc.FileList;
            Assert.That(files.Count(f => f.IsMetadata), Is.GreaterThan(0));
        }

        [Test]
        public async Task ThatFolderWithMetadataFilesDetectsThemAndMapsThem()
        {
            var startAt = @"C:\develop\StructImporter\SampleStruct1\FolderA";
            var fc = new FolderContentManager(startAt);
            var files = fc.FileList;
            var fileName = "cat-page.html";
            Assert.That(files.Count(f => f.FileName == fileName) > 0);
            Assert.That(files.Count(f => f.FileName == fileName) == 2);
            Assert.That(files.Count(f => f.FileName == fileName && f.IsMetadata) == 1);
            Assert.That(files.Count(f => f.FileName == fileName && !f.IsMetadata) == 1);
        }

        [Test]
        public async Task ThatFolderWithMetadataMapLoadsTheSectionId()
        {
            var startAt = @"C:\develop\StructImporter\SampleStruct1\FolderA";
            var fc = new FolderContentManager(startAt);
            Assert.True(fc.SectionId == 33);
        }

        [Test]
        public async Task ThatFolderWithMetadataMapLoadsTypeLists()
        {
            var startAt = @"C:\develop\StructImporter\SampleStruct1\FolderA";
            var fc = new FolderContentManager(startAt);
            Assert.That(fc.DefaultPageTypeName, Is.Not.Empty);
        }
        [Test]
        public async Task ThatFolderWithMetadataMapLoadsBuildTypeLists()
        {
            var startAt = @"C:\develop\StructImporter\SampleStruct1\FolderA";
            var fc = new FolderContentManager(startAt);
            Assert.That(fc.BuildPageTypeList, Is.Not.Empty);
        }

        [Test]
        public async Task ThatFolderSeesRtfFiles()
        {

            var startAt = @"C:\develop\StructImporter\SampleStruct1\TestCases\FolderA";
            var fc = new FolderContentManager(startAt);
            Assert.That(fc.FileList.Any(f=>f.Extension==".rtf"));
        }
        [Test]
        public async Task ThatFolderSeesExcelFiles()
        {

            var startAt = @"C:\develop\StructImporter\SampleStruct1\TestCases\FolderA";
            var fc = new FolderContentManager(startAt);
            Assert.That(fc.FileList.Any(f => f.Extension == ".xlsx"));
        }
        [Test]
        public async Task ThatFolderSeesExcelPath()
        {

            var startAt = @"C:\develop\StructImporter\SampleStruct1\TestCases\FolderA";
            var fc = new FolderContentManager(startAt);
            var xl = fc.FileList.First(f => f.Extension == ".xlsx");
            var fill = Path.Combine(startAt, "New Microsoft Excel Worksheet.xlsx");
            Assert.That(xl.FullPath, Is.EqualTo(fill));
        }
        [Test]
        public async Task ThatFolderSeesExcelSize()
        {

            var startAt = @"C:\develop\StructImporter\SampleStruct1\TestCases\FolderA";
            var fc = new FolderContentManager(startAt);
            var xl = fc.FileList.First(f => f.Extension == ".xlsx");
            Assert.That(xl.Size, Is.GreaterThan(6000));
        }
        [Test]
        public async Task ThatFolderSeesExcelNames()
        {

            var startAt = @"C:\develop\StructImporter\SampleStruct1\TestCases\FolderA";
            var fc = new FolderContentManager(startAt);
            var xl = fc.FileList.First(f => f.Extension == ".xlsx");
            Assert.That(xl.FileName, Is.EqualTo("New Microsoft Excel Worksheet.xlsx"));
            Assert.That(xl.RawName, Is.EqualTo("New Microsoft Excel Worksheet.xlsx"));
            
        }
    }
}
