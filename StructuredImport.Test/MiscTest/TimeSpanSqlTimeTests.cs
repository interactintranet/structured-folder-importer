﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using NUnit.Framework;

namespace StructuredImport.Test.MiscTest
{
    [TestFixture]
    internal class TimeSpanSqlTimeTests
    {
        [Test]
        public void Test1()
        {
            var c = TestHelper.GetTestConfig();
            var db = new SqlConnection(c.MetadataDatabaseConnectionString);
            var ts1 = new TimeSpan(0, 0, 10, 15);
            db.Execute("INSERT INTO dbo.TimeTest (Duration1) VALUES (@Duration)", new
            {
               Duration = ts1
            });
        }
    }
}
