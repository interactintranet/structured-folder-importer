﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using StructuredImport.Core.App;
using StructuredImport.Core.Model.InteractPage;

namespace StructuredImport.Test.MiscTest
{
    [TestFixture]
    class PayloadLoaderTests
    {
        [Test]
        public async Task LoadThem()
        {

            var config = TestHelper.GetTestConfig();
            var pageLoaded = new PageMetadataExtractor(config);
            await pageLoaded.LoadAllPages();
        }
    }
}
