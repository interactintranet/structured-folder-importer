﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using StructuredImport.Core.App;
using StructuredImport.Core.Model;

namespace StructuredImport.Test
{
    [TestFixture]
    public class ContentExistenceCheckerTests
    {
        [Test]
        public async Task ThatMetadataCheckReturnsFalseWhenNotInTable()
        {
            var tc = TestHelper.GetTestConfig();
            var sut = new ContentExistenceChecker(tc);
            var res = await sut.CheckByMetadata(@"c:\temp\a.txt", "temp", 1, 1, "", DateTime.MinValue);
            Assert.That(res, Is.EqualTo(false));
        }

        //[Test]
        //public async Task ThatMetadataChecKReturnsTrueForItemInTable()
        //{
        //    var tc = TestHelper.GetTestConfig();
        //    var sut = new ContentExistenceChecker(tc);
        //    var res = await sut.CheckByMetadata("DummyFileName", @"DummyTestFolder",  null);
        //    Assert.That(res, Is.EqualTo(true));
        //}

        private AppConfig _config;
        private ContentExistenceChecker _checker;

        [OneTimeSetUp]
        public void SetUp()
        {
            _config = TestHelper.GetTestConfig();
            _checker = new ContentExistenceChecker(_config);
        }

        [Test]
        public async Task ThatCheckByMetadataFindsAFile()
        {

            var r = await _checker.CheckByMetadata1("DummyFileName", "DummyTestFolder", -3, -4, "c:\\temp\\a.txt",
                DateTime.Now);

            Assert.That(r.FileIsOnTarget, Is.True);
        }
        [Test]
        public async Task ThatCheckByMetadataFindsAFileIsOlderIfWriteDateIsToday()
        {
            //File is older than the record in the table so an update should be flagged
            var r = await _checker.CheckByMetadata1("DummyFileName", "DummyTestFolder", -3, -4, "c:\\temp\\a.txt",
                DateTime.Now);

            Assert.That(r.FileIsOnTarget, Is.True);
            Assert.That(r.MetadataRecordIsOlder, Is.True);
            Assert.That(r.UpdateIsRequired, Is.True);
        }
        [Test]
        public async Task ThatCheckByMetadataFindsAFileIsNotOlderIfWriteDateIsInThePast()
        {
            //File is 
            var r = await _checker.CheckByMetadata1("DummyFileName", "DummyTestFolder", -3, -4, "c:\\temp\\a.txt",
                new DateTime(2022,2, 7, 12, 0,0));

            Assert.That(r.FileIsOnTarget, Is.True);
            Assert.That(r.MetadataRecordIsOlder, Is.False);
            Assert.That(r.UpdateIsRequired, Is.False);
        }
    }
}
