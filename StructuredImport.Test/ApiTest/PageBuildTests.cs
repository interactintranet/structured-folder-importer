﻿using System;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NUnit.Framework;
using StructuredImport.Core.App;
using StructuredImport.Core.Model;
using StructuredImport.Core.Model.AppPage;
using StructuredImport.Core.Model.InteractPage;

namespace StructuredImport.Test.ApiTest
{
    [TestFixture]
    public class PageBuildTests
    {
        private TokenHolder _tokenHolder;
        private AppConfig _testConfig;

        [SetUp]
        public void SetUpTests()
        {
            _testConfig = TestHelper.GetTestConfig();
            _tokenHolder = new TokenHolder(_testConfig);
            var t = _tokenHolder.CurrentToken; //force a token fetch
        }

        [Test]
        public async Task ThatABasicPageIsCreated()
        {

            var token = _tokenHolder.CurrentToken;
            var pb = new PageBuilder(_testConfig);

            var p = pb.BuildPagePrototype();
            p.Page.Title = $"Created by Test : {DateTime.Now}";
            p.Page.TopSectionIds = new[] {3170}; //"Content Area X"
            p.Page.CategoryIds = new[] {3174}; //"Railways"
            p.Page.AuthorId = 1276; //"Cara Ewen"

            var result = await pb.CreatePageFromPayload(p, token);

            Console.WriteLine(result.FailMessage);
            Assert.That(result.PageWasCreated);

        }

        [Test, Ignore("Evolutionary test")]
        public async Task ThatAnUploadPageIsCreated()
        {
            var token = _tokenHolder.CurrentToken;
            var pb = new PageBuilder(_testConfig);

            var p = pb.BuildPagePrototype();
            p.Page.Title = $"Created by Test : {DateTime.Now}";
            p.Page.TopSectionIds = new[] {3170}; //"Content Area X"
            p.Page.CategoryIds = new[] {3174}; //"Railways"
            p.Page.AuthorId = 1276; //"Cara Ewen"
            var uploadFilePath = @"C:\temp-rga\Searchterms.docx";
            var fileGuid = Guid.NewGuid().ToString();
            var size = new FileInfo(uploadFilePath).Length;
            var paf = new PageAttachmentFeature
            {
                FileSize = Convert.ToInt32(size),
                Address = uploadFilePath,
                Title = "Search Terms",
                Target = uploadFilePath,
                FileGuid = fileGuid,
                Extension = ".docx",
                GoStraightToTarget = false,
                DisableDownload = false
            };
            p.Page.PageAttachments.Files = new PageAttachmentFeature[] {paf};
            p.Page.Content.Upload = paf.MapToUpload();
            p.Page.ContentType = "upload";
            var result = await pb.CreatePageFromPayload(p, token);

            Console.WriteLine(result.FailMessage);
            Assert.That(result.PageWasCreated);
        }



        [Test, Ignore("Evolutionary Test")]
        public async Task ThatAssetIdPostTakesADocumentAndUploadsIt()
        {
            var sourceFile = @"C:\temp-rga\Searchterms.docx";
            var bytes = File.ReadAllBytes(sourceFile);
            var url = $"{_testConfig.InteractApiUrl}/api/asset/upload/composer";
            var fileName = $"File_{DateTime.Now.Second}";
            var r = await InteractApiHandler.PostUpload(url, bytes, fileName, _testConfig.InteractTenantId, _tokenHolder.CurrentToken);

            var bodyResponse = JsonConvert.DeserializeObject<UploadResponseBody>(r.Body);

            Assert.That(r.Header.IsSuccessStatusCode);
            Assert.That(bodyResponse.FileInfo.FileUploadAttachment.Title, Is.EqualTo(fileName));

        }

        [Test]
        public async Task ThatPageBuilderUploadMethodCompletes()

        {
            var pb = new PageBuilder(_testConfig);
            var sourceFile = @"C:\temp-rga\Searchterms.docx";
            var r = await pb.UploadFileToFileStorage(sourceFile);
            Assert.That(r.UploadAttachment.Title, Is.EqualTo("Searchterms.docx"));
        }

        [Test]
        public async Task ThatPageBuilderCreatesPageWithAttachmentWithMinimumRequiredInfo()
        {
            var pb = new PageBuilder(_testConfig);

            var pci = new PageCreateInfo();
            pci.FileLocation = @"C:\temp-rga\Searchterms.docx";

            pci.Title = $"Created by Test : {DateTime.Now}";
            pci.TopSectionIds = new[] { 3170 }; //"Content Area X"
            pci.CategoryIds = new[] { 3174 }; //"Railways"
            pci.AuthorId = 1276; //"Cara Ewen"
            var r = await pb.CreateUploadPage(pci);

            Console.WriteLine(r.FailMessage);
            Assert.That(r.PageWasCreated);

        }
        [Test]
        public async Task ThatPageBuilderCreatesPageWithAttachmentWitSummary()
        {
            var pb = new PageBuilder(_testConfig);

            var pci = new PageCreateInfo();
            pci.FileLocation = @"C:\temp-rga\Searchterms.docx";

            pci.Title = $"Created by Test : {DateTime.Now}";
            pci.TopSectionIds = new[] { 3170 }; //"Content Area X"
            pci.CategoryIds = new[] { 3174 }; //"Railways"
            pci.AuthorId = 1276; //"Cara Ewen"
            pci.Summary = "Override the default summary";

            var r = await pb.CreateUploadPage(pci);

            Console.WriteLine(r.FailMessage);
            Assert.That(r.PageWasCreated);

        }
        [Test]
        public async Task ThatPageBuilderCreatesPageWithAttachmentWithAdditionalKeywords()
        {
            var pb = new PageBuilder(_testConfig);

            var pci = new PageCreateInfo();
            pci.FileLocation = @"C:\temp-rga\Searchterms.docx";

            pci.Title = $"Created by Test : {DateTime.Now}";
            pci.TopSectionIds = new[] { 3170 }; //"Content Area X"
            pci.CategoryIds = new[] { 3174 }; //"Railways"
            pci.AuthorId = 1276; //"Cara Ewen"
            pci.Summary = "Keywords added";
            pci.Keywords = new[] {"Station", "Signal"};
            //pci.AssetId = pci.GetAssetIdFromDocType();

            var r = await pb.CreateUploadPage(pci);

            Console.WriteLine(r.FailMessage);
            Assert.That(r.PageWasCreated);

        }

        [Test]
        public async Task ThatPublishedAsAuthorIsRespected()
        {
            var token = _tokenHolder.CurrentToken;
            var pb = new PageBuilder(_testConfig);

            var p = pb.BuildPagePrototype();
            p.Page.Title = $"Created by Test : {DateTime.Now}";
            p.Page.TopSectionIds = new[] { 3170 }; //"Content Area X"
            p.Page.CategoryIds = new[] { 3174 }; //"Railways"
            p.Page.AuthorId = 1276; //"Cara Ewen"
            p.Page.PublishAsId = 276; //RG

            var result = await pb.CreatePageFromPayload(p, token);

            Console.WriteLine(result.FailMessage);
            Assert.That(result.PageWasCreated);
        }
        [Test]
        public async Task ThatPublishedDateIsRespected()
        {
            var token = _tokenHolder.CurrentToken;
            var pb = new PageBuilder(_testConfig);

            var p = pb.BuildPagePrototype();
            p.Page.Title = $"Created by Test : {DateTime.Now}";
            p.Page.TopSectionIds = new[] { 3170 }; //"Content Area X"
            p.Page.CategoryIds = new[] { 3175 }; //"Amphibians"
            p.Page.AuthorId = 1276; //"Cara Ewen"
            p.Page.PubStartDate = DateTime.Now.AddDays(-90);
            p.Page.PubEndDate = p.Page.PubStartDate.AddMonths(_testConfig.PageLifetimeInMonths);
            p.Page.ReviewDate = p.Page.PubEndDate.AddMonths(_testConfig.PageReviewOffsetInMonths);
            var result = await pb.CreatePageFromPayload(p, token);

            Console.WriteLine(result.FailMessage);
            Assert.That(result.PageWasCreated);
        }
    }
}
