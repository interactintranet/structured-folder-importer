﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NUnit.Framework;
using StructuredImport.Core.App;
using StructuredImport.Core.Model.InteractPage.PageDownload;

namespace StructuredImport.Test.ApiTest
{
    [TestFixture]
    class PageUpdateTests
    {
        [Test]
        public async Task ThatGetPageDataGetsData()
        {
            var config = TestHelper.GetTestConfig();
            var pageBuilder = new PageBuilder(config);
            var pageId = 3252;
            var pd = await pageBuilder.FetchPage(pageId);

            var bodyContent = JsonConvert.SerializeObject(pd);
            var f = File.CreateText(@$"C:\temp-rga\page-{pageId}.json");
            f.Write(bodyContent);
            f.Close();

        }

        [Test]
        public async Task ThatGetPageDataGetsData1()
        {
            var config = TestHelper.GetTestConfig2();
            var pageBuilder = new PageBuilder(config);
            var pageId = 4855;
            var py = await pageBuilder.FetchPagePayload(pageId);
            //var pd = await pageBuilder.FetchPage(pageId);
            //Write a file with the payload....
            //var bodyContent = JsonConvert.SerializeObject(py);
            var f = File.CreateText(@$"C:\temp-rga\page-py-{pageId}.json");
            f.Write(py);
            f.Close();

        }

        [Test]
        public async Task ThatGetPageDataMapsTopSectionId()
        {
            var config = TestHelper.GetTestConfig();
            var pageBuilder = new PageBuilder(config);

            var pd = await pageBuilder.FetchPage(3252);
            var pu = PageBuilder.Map(pd);

            CollectionAssert.AreEqual(pu.Page.TopSectionIds, pd.TopSectionIds);

        }

        [Test]
        public async Task ThatUpdatedAlpaca2()
        {
            var config = TestHelper.GetTestConfig();
            var pageBuilder = new PageBuilder(config);

            await pageBuilder.UpdateUploadPage(3252,
                @"C:\develop\StructImporter\SampleStruct1\TestCases\SpacedFolders\alpaca2.pdf");


        }

        [Test]
        public async Task ThatUpdatedMousePageWithTranslation()
        {
            ;

            var config = TestHelper.GetTestConfig();
            var tokenHolder = new TokenHolder(config);
            var pageBuilder = new PageBuilder(config);
            var pageId = 3270;
            var pd = await pageBuilder.FetchPage(pageId);
            var t1 = new Translation
            {
                languageId = 10,
                title = "Mace",
                body = "maces",
                summary = "maces upafes"
            };
            var ts = new TranslationsItem
            {
                originalLanguageId = 0,
                translations = new[] { t1 }
            };

            var pu = PageBuilder.Map(pd);
            pu.Page.Translations = ts;
            pu.Transition.State = "published";
            await pageBuilder.UpdatePageFromPayload(pu, pageId, tokenHolder.CurrentToken);

            //turns out we need to have all the translations in the PageUpload otherwise the existing ones are blown away
            //The API doesn't provide any translations on the GET call though

        }

        [Test]
        public async Task SavePagePayloadFromGet()
        {
            var config = TestHelper.GetTestConfig();
            var tokenHolder = new TokenHolder(config);
            var pageBuilder = new PageBuilder(config);
            var pageId = 3270;
            var payld = await pageBuilder.FetchPagePayload(pageId);
            var f = File.CreateText(@$"C:\temp-rga\page-py-{pageId}.json");
            f.Write(payld);
            f.Close();
        }
        [Test]
        public async Task SavePagePayloadFromGet2()
        {
            var config = TestHelper.GetTestConfig2();
            var tokenHolder = new TokenHolder(config);
            var pageBuilder = new PageBuilder(config);
            var pageId = 4855;
            var payld = await pageBuilder.FetchPagePayload(pageId);
            var f = File.CreateText(@$"C:\temp-rga\page-py-{pageId}.json");
            f.Write(payld);
            f.Close();
        }
    }
}
