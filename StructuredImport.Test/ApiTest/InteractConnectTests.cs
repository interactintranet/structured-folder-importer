﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using StructuredImport.Core.App;

namespace StructuredImport.Test.ApiTest
{
    [TestFixture]
    class InteractConnectTests
    {
        [Test]
        public async Task ThatApiWillConnect()
        {
            var testConfig = TestHelper.GetTestConfig();
            var tokenHelper = new TokenHolder(testConfig);
            var token = tokenHelper.CurrentToken;

            var url = $"{testConfig.InteractApiUrl}/api/people";
            var data = await InteractApiHandler.Get(url, testConfig.InteractTenantId, token);

            Assert.That(data.Header.IsSuccessStatusCode, Is.True);
        }
    }
}
