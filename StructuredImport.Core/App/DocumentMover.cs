﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using StructuredImport.Core.Interfaces;
using StructuredImport.Core.Model;
using StructuredImport.Core.Model.AppPage;
using StructuredImport.Core.Model.Database;
using StructuredImport.Core.Support;

namespace StructuredImport.Core.App
{
    /// <summary>
    /// Simple processor to move non-html documents to Interact as upload pages
    /// </summary>
    public class DocumentMover : IImportProcessor
    {
        private readonly AppConfig _appConfig;
        private readonly SqlConnection _dbMetadata;
        private readonly string _correlationId;
        private readonly DbLogger _logger;
        private readonly List<ExtensionPageType> _extTypes;
        private readonly PageBuilder _pageBuilder;

        public DocumentMover(AppConfig appConfig)
        {
            _appConfig = appConfig;
            _dbMetadata = new SqlConnection(_appConfig.MetadataDatabaseConnectionString);
            _correlationId = Guid.NewGuid().ToString().Substring(0, 8);
            _logger = new DbLogger(appConfig, _dbMetadata, _correlationId);
            _pageBuilder = new PageBuilder(appConfig);

            _extTypes = _dbMetadata.Query<ExtensionPageType>("SELECT * FROM Migrate.ExtensionPageType").ToList();
        }

        public async Task<ImportProcessorResult> RunProcess(bool dryRun )
        {
            var result = new ImportProcessorResult();
            _logger.LogWrite("RunProcess started", LogEventType.Commencement);
            var rf = _dbMetadata.QuerySingle<string>("SELECT RootFolderPath FROM Core.RootFolder WHERE RootFolderPathId=0");
            var queueQry = $"SELECT SF.SourceFolderId,SF.FolderName,SF.RelativePath,SF.ParentId, FD.SectionId, FD.CategoryId FROM Core.SourceFolder SF INNER JOIN Migrate.SourceFolderDestination FD ON FD.SourceFolderId = SF.SourceFolderId WHERE SF.FileCount>0";
            var queue = (await _dbMetadata.QueryAsync<SourceFolderDestination>(queueQry)).ToList();
            foreach (var folder in queue)
            {
                await ProcessFilesInFolder(rf,folder, dryRun);
            }

            _logger.LogWrite($"RunProcess finished", LogEventType.Termination);

            return result;
        }

        private async Task ProcessFilesInFolder(string root, SourceFolderDestination folder, bool dryRun )
        {
            var files = _dbMetadata.Query<SourceFile>($"SELECT * FROM Core.SourceFile WHERE FolderId={folder.SourceFolderId}");
            foreach (var fileInfo in files)
            {
                var fileProcessedCheck = await CheckProcessedState(fileInfo);
                if (fileProcessedCheck.IsProcessed)
                {
                    _logger.LogWrite($"File {fileInfo.SourceFileId} has been processed - {fileInfo.FileName}");
                    continue;
                }
                //if file is html 
                var filePermitted = await CheckFileType(fileInfo.Extension);
                if (!filePermitted)
                {
                    _logger.LogWrite($"File type not processed  {fileInfo.Extension}");
                    continue;
                }

                if (dryRun)
                {
                    await DryRunReport(fileInfo, root, folder);
                }
                else
                {
                    await UploadFileToInteract(fileInfo, root, folder);
                    await Task.Delay(100);
                }
            }
        }

        private async Task DryRunReport(SourceFile fileInfo, string root, SourceFolderDestination folder)
        {
            Console.WriteLine($"Candidate: {fileInfo.FileName} will be processed");
        }

        private async Task ProcessFilesInFolderE(string root,SourceFolderDestination folder)
        {
            var pth = string.Concat(root, folder.RelativePath); // Path.Combine(root, folder.RelativePath);
            var filesInFolder = (new DirectoryInfo(pth)).EnumerateFiles();
            foreach (var fileInfo in filesInFolder)
            {
               //if file has been processed
               var fileProcessedCheck = await CheckProcessedState(fileInfo);
               if (fileProcessedCheck.IsProcessed) continue;
               //if file is html 
               var filePermitted = await CheckFileType(fileInfo);
               if (!filePermitted)
               {
                   _logger.LogWrite($"File type not processed  {fileInfo.Extension}");
                   continue;
               }

               await UploadFileToInteract(fileInfo, folder);
            }
        }

        private async Task UploadFileToInteract(SourceFile fileInfo, string root, SourceFolderDestination sourceFolderDestination)
        {
            var buildInfo = new PageCreateInfo();
            buildInfo.TopSectionIds = new[] { sourceFolderDestination.SectionId };
            buildInfo.CategoryIds = new[] { sourceFolderDestination.CategoryId };
            buildInfo.FileLocation = string.Concat(root, sourceFolderDestination.RelativePath, "\\", fileInfo.FileName);
            buildInfo.AuthorId = _appConfig.DefaultAuthorId;
            buildInfo.Title = fileInfo.FileName;
            buildInfo.Summary = fileInfo.FileName;
            buildInfo.LastUpdated = DateTime.Now;
            buildInfo.Keywords = new string[] { sourceFolderDestination.FolderName };
            var pr = await _pageBuilder.CreateUploadPage(buildInfo);
            if (pr.PageWasCreated)
            {
                await WriteSuccess(pr, fileInfo);
                _logger.LogWrite($"Page {pr.PageId} created - {fileInfo.FileName}");
            }
            else
            {
                _logger.LogWrite(pr.FailMessage, LogEventType.Warning);
            }
        }

        private async Task WriteSuccess(PageCreateResult result, SourceFile fileInfo)
        {
            await _dbMetadata.ExecuteAsync("INSERT INTO Migrate.ContentPageTrack(SourceFileId ,[IsComplete],[PageId],[CompletedStamp])VALUES(@SourceFileId ,@IsComplete,@PageId,SysDateTime())", new
            {
                SourceFileId = fileInfo.SourceFileId,
                IsComplete = true,
                PageId = result.PageId
            });
        }

        private async Task UploadFileToInteract(FileInfo fileInfo, SourceFolderDestination sourceFolderDestination)
        {
            //
            var buildInfo = new PageCreateInfo();
            buildInfo.TopSectionIds = new[] { sourceFolderDestination.SectionId };
            buildInfo.CategoryIds = new[] { sourceFolderDestination.CategoryId };
            buildInfo.FileLocation = fileInfo.FullName;
            buildInfo.AuthorId = _appConfig.DefaultAuthorId;
            buildInfo.Title = fileInfo.Name;
            buildInfo.Summary = fileInfo.Name;
            buildInfo.LastUpdated  = DateTime.Now;
            buildInfo.Keywords = new string[] {  sourceFolderDestination.FolderName };
            var pr = await _pageBuilder.CreateUploadPage(buildInfo);
            if (pr.PageWasCreated)
            {
                await WriteSuccess(fileInfo.FullName, pr);
            }
            else
            {
                _logger.LogWrite(pr.FailMessage, LogEventType.Warning);
            }

        }

        private async Task WriteSuccess(string path, PageCreateResult result)
        {
            await  _dbMetadata.ExecuteAsync("INSERT INTO Migrate.ContentPageTrack(SourceFileId ,[IsComplete],[PageId],[CompletedStamp])VALUES(@SourcePath ,@IsComplete,@PageId,SysDateTime())", new
                    {
                        SourceFileId = path, IsComplete= true, PageId = result.PageId 
                    });
           
        }
        private async Task<bool> CheckFileType(string ext)
        {
            var check = _extTypes.SingleOrDefault(d => d.Extension == ext);
            if (check == null) return false;
            return check.IsLegal;
        }
        private async Task<bool> CheckFileType(FileInfo fileInfo)
        {
            return await CheckFileType(fileInfo.Extension);
            var check = _extTypes.SingleOrDefault(d => d.Extension == fileInfo.Extension);
            if (check == null) return false;
            return check.IsLegal;
        }

        private async Task<ProcessedCheckResult> CheckProcessedState(FileInfo fileInfo)
        {
            var f = fileInfo.FullName;
            var check = await _dbMetadata.QueryAsync($"SELECT * FROM Migrate.ContentPageTrack WHERE SourcePath=@Path And IsComplete=1", new {Path = fileInfo.FullName});
            return new ProcessedCheckResult
            {
                IsProcessed = check.Any()
            };
        }
        private async Task<ProcessedCheckResult> CheckProcessedState(SourceFile fileInfo)
        {
            //var f = fileInfo.FullName;
            var check = await _dbMetadata.QueryAsync($"SELECT * FROM Migrate.ContentPageTrack WHERE SourceFileId=@fileId And IsComplete=1", new { FileId = fileInfo.SourceFileId });
            return new ProcessedCheckResult
            {
                IsProcessed = check.Any()
            };
        }
    }

    internal class SourceFile
    {
        public int SourceFileId { get; set; }
        public string Extension { get; set; }
        public string FileName { get; set; }
    }

    internal class SourceFolderDestination : SourceFolder
    {
        public int SectionId { get; set; }
        public int  CategoryId { get; set; }
    }

    internal class ProcessedCheckResult
    {
        public bool IsProcessed { get; set; }
    }
    internal class ExtensionPageType
    {
        public string Extension { get; set; }
        public int PageTypeId { get; set; }
        public string PageTypeDesc { get; set; }
        public bool IsLegal { get; set; }

    }
}
