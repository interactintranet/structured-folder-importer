﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using StructuredImport.Core.Model;
using StructuredImport.Core.Model.Database;
using StructuredImport.Core.Support;

namespace StructuredImport.Core.App
{
    internal class ContentExistenceChecker
    {
        private readonly AppConfig _appConfig;
        private SqlConnection _dbMetadata;

        public ContentExistenceChecker(AppConfig appConfig)
        {
            _appConfig = appConfig;
            _dbMetadata = new SqlConnection(_appConfig.MetadataDatabaseConnectionString);
        }

        public async Task<ExistenceCheckResult> FileCheckOnTarget(FileContentItem contentItem, FolderInfo folderInfo)
        {
            //var ec = new ExistenceCheckResult();
            if (_appConfig.ExistenceCheckByMetadata)
            {
                return await CheckByMetadata1(contentItem.FileName, folderInfo.FolderName, folderInfo.SectionId, folderInfo.CategoryId, contentItem.FullPath, contentItem.LastUpdatedStamp);
            }
            if (_appConfig.ExistenceCheckByQueryingSite)
            {
                throw new NotSupportedException();
            }
            throw new NotSupportedException();
            
            //return ec;
        }

        //public async Task<bool> FileExistsOnTarget(FileContentItem contentItem, FolderInfo folderInfo)
        //{
        //    var exists = false;
        //    if (_appConfig.ExistenceCheckByMetadata)
        //    {
        //        //exists = await CheckByMetadataOld(contentItem.FileName, folderInfo.FolderName, folderInfo.FolderId);
        //        exists = await CheckByMetadata(contentItem.FileName, folderInfo.FolderName, folderInfo.SectionId, folderInfo.CategoryId, contentItem.FullPath, contentItem.LastUpdatedStamp);
        //    }

        //    if (_appConfig.ExistenceCheckByQueryingSite)
        //    {
        //        throw new NotSupportedException();
        //    }

        //    return exists;
        //}

        internal async Task<ExistenceCheckResult> CheckByMetadata1(string fileName, string folderName, int sectionId, int categoryId, string fullPath, DateTime contentItemLastUpdated)
        {
            var check = await _dbMetadata.QuerySingleOrDefaultAsync<ProcessCheck>("Core.FileHasBeenProcessedCheck @FileName, @FolderName, @SectionId, @CategoryId, @FullPath", new
            {
                FileName = fileName,
                FolderName = folderName,
                SectionId = sectionId,
                CategoryId = categoryId,
                FullPath = fullPath
            });
            if (check == null) return new ExistenceCheckResult();
            return new ExistenceCheckResult
            {
                FileIsOnTarget = true,
                MetadataRecordIsOlder = IsMetadataRecordOlder(check.FileLastWriteStamp, contentItemLastUpdated), // ,
                FileImportTrackId = check.FileImportTrackId,
                PageId = check.PageId
            };
        }

        private bool IsMetadataRecordOlder(DateTime fileLastWriteStamp, DateTime contentItemLastUpdated)
        {
            return fileLastWriteStamp.Truncate(TimeSpan.FromSeconds(1)) < contentItemLastUpdated.Truncate(TimeSpan.FromSeconds(1));
        }

        internal async Task<bool> CheckByMetadata(string fileName, string folderName, int sectionId, int categoryId, string fullPath, DateTime contentItemLastUpdated)
        {
            var check = await _dbMetadata.QueryAsync("Core.FileHasBeenProcessedCheck @FileName, @FolderName, @SectionId, @CategoryId, @FullPath", new
            {
                FileName = fileName,
                FolderName = folderName,
                SectionId = sectionId,
                CategoryId = categoryId,
                FullPath = fullPath
            });
            return check.Any();
        }

        //internal async Task<bool> CheckByMetadataOld(string fileName, string folderName, int folderId)
        //{
            
        //    var check = await _dbMetadata.QueryAsync("Core.FileHasBeenProcessedCheck @FileName, @FolderName, @FolderId", new
        //    {
        //        FileName = fileName, FolderName = folderName, FolderId = folderId
        //    });
        //    return check.Any();
        //}
    }

    internal class ProcessCheck
    {
        public  int PageId { get; set; }
        public DateTime FileLastWriteStamp { get; set; }
        public int FileImportTrackId { get; set; }
    }
}