﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using StructuredImport.Core.Model;
using StructuredImport.Core.Support;

namespace StructuredImport.Core.App
{
    internal class PageMetadataExtractor
    {
        private AppConfig _appConfig;
        private SqlConnection _dbMetadata;
        private TokenHolder _tokenHolder;
        private string _correlationId;
        private DbLogger _logger;

        public PageMetadataExtractor(AppConfig appConfig)
        {
            _appConfig = appConfig;
            _dbMetadata = new SqlConnection(_appConfig.MetadataDatabaseConnectionString);
            _tokenHolder = new TokenHolder(_appConfig);
            _correlationId = Guid.NewGuid().ToString().Substring(0, 8);
            _logger = new DbLogger(_appConfig, _dbMetadata, _correlationId);
        }
        /// <summary>
        /// Walks through all the pages and gets their page metadata from the API and puts it in the PagePayload table.
        /// This is for analysis of pages, not directly used by the app currently.
        /// </summary>
        /// <returns></returns>
        public async Task LoadAllPages()
        {
            var pages = _dbMetadata.Query<PagePayload>(
                "SELECT PageId FROM Core.FileImportTrack EXCEPT SELECT PageId FROM Core.PagePayload").ToList();
            var token = _tokenHolder.CurrentToken;

            var url = $"{_appConfig.InteractApiUrl}/page/X+X/composer/latest";
            
            foreach (var page in pages.Take(100))
            {
                url = $"{_appConfig.InteractApiUrl}api/page/{page.PageId}/composer/latest";
                var data = await InteractApiHandler.Get(url, _appConfig.InteractTenantId, token);
                if (data.Header.IsSuccessStatusCode)
                {
                    await _dbMetadata.ExecuteAsync("INSERT INTO Core.PagePayload (PageId,PagePayload,RTCreateStamp)VALUES(@PageId, @Payload, SysDateTime())",
                        new
                        {
                            page.PageId, Payload = data.Body
                        });
                }
                else
                {
                    _logger.LogWrite($"Page {page.PageId} not found ", LogEventType.Warning);
                }
            }
            
        }
    }

    internal class PagePayload
    {
        public int PageId { get; set; }
    }
}
