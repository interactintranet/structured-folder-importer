﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using StructuredImport.Core.Interfaces;
using StructuredImport.Core.Model;
using StructuredImport.Core.Model.AppPage;
using StructuredImport.Core.Model.Database;
using StructuredImport.Core.Support;

namespace StructuredImport.Core.App
{
    public class ImportProcessorDatabaseDriven :  IImportProcessor
    {
        private readonly AppConfig _appConfig;
        private readonly SqlConnection _dbMetadata;
        private readonly PageBuilder _pageBuilder;
        private readonly ContentExistenceChecker _checker;
        private readonly string _correlationId;
        private readonly DbLogger _logger;
        private readonly IMetadataTransfer _metadataTransfer;
        public ImportProcessorDatabaseDriven(AppConfig appConfig) 
        {
            _appConfig = appConfig;
            _dbMetadata = new SqlConnection(_appConfig.MetadataDatabaseConnectionString);
            _pageBuilder = new PageBuilder(_appConfig);
            _checker = new ContentExistenceChecker(_appConfig);
            _correlationId = Guid.NewGuid().ToString().Substring(0, 8);
            _logger = new DbLogger(appConfig, _dbMetadata, _correlationId);
        }
        /// <summary>
        /// Finds all the folders in TargetLocation table that are marked for processing (IsActive=true) and that have a mapping set up (SectionId/CategoryId).
        /// Looks in each folder, for each file, it checks to see if we have imported it to the mapped category, if not, it creates a page and uploads the file.
        /// Uploaded files are tracked in the FileImportTrack table.
        /// </summary>
        /// <returns></returns>
        public async Task<ImportProcessorResult> RunProcess(bool dryRun = false)
        {
            //NB: DryRun not implemented for this process
            _logger.LogWrite("RunProcess started", LogEventType.Commencement);
            //get list of folders from database
            var folderList = (await _dbMetadata.QueryAsync<FolderInfo>("Core.PendingFoldersForProcessingFetch ")).ToList();
            //process each folder
            _logger.LogWrite($"Found {folderList.Count} folders to check");
            foreach (var folderInfo in folderList.ToList())
            {
                LogFolder(folderInfo);
                if (!ValidateFolderInfo(folderInfo)) continue;
                await ProcessFilesInFolder(folderInfo);
            }
            _logger.LogWrite($"RunProcess finished", LogEventType.Termination);
            return new ImportProcessorResult();
        }

        private bool ValidateFolderInfo(FolderInfo folderInfo)
        {
            var valid = folderInfo.SectionId>0 && folderInfo.CategoryId>0;
            if (!valid)
            {
                _logger.LogWrite($"Folder {folderInfo.FolderName} has no Section/Category mapping");
            }
            return valid;
        }

        internal async Task<ProcessFileInFolderResult> ProcessFilesInFolder(FolderInfo folderInfo)
        {
            int existed = 0, completed = 0, failed = 0, updated=0;
            var folderData = new  FolderContentManager(folderInfo.FolderFullPath);
            var eligibleFiles = folderData.FileList.ToList();
            LogProcessFilesInFolder(folderInfo, eligibleFiles.Count);
            
            foreach (var contentItem in eligibleFiles)
            {
                if (!ValidateFileType(contentItem))
                {
                    failed++;
                    LogFileAsIncompatible(contentItem, folderInfo);
                    continue;
                }

                EnrichMetadataOnContent(contentItem, folderInfo);
                var existenceCheck = await CheckFileAgainstTarget(contentItem, folderInfo);
                if (existenceCheck.FileIsOnTarget && !existenceCheck.UpdateIsRequired)
                {
                    existed++;
                    continue;
                }
                
                if (existenceCheck.UpdateIsRequired)
                {
                    var updatedResult = await UpdateUploadPage(existenceCheck.PageId, contentItem, folderInfo);
                    if (updatedResult.PageWasUpdated)
                    {
                        await MarkPageAsUpdated(contentItem, folderInfo, updatedResult, existenceCheck.FileImportTrackId);
                        updated++;
                    }
                    else
                    {
                        await MarkPageUpdateFailed(contentItem, folderInfo, updatedResult);
                        failed++;
                    }
                    continue;
                }
                
                var built = await BuildUploadPage(contentItem, folderInfo);
                if (built.PageWasCreated)
                {
                    await MarkPageAsCreated(contentItem, folderInfo, built);
                    completed++;
                }
                else
                {
                    MarkPageCreationFailed(contentItem, folderInfo, built);
                    failed++;
                }
                
            }
            LogProcessedFilesInFolder(folderInfo, completed, failed, existed, eligibleFiles.Count, updated);
            return new ProcessFileInFolderResult();
        }

        private void EnrichMetadataOnContent(FileContentItem contentItem, FolderInfo folderInfo)
        {
            //NO-OP
            //Here we would fetch info like AuthorId, Created and Modified dates if we have access to them
        }

        private async Task<ExistenceCheckResult> CheckFileAgainstTarget(FileContentItem contentItem, FolderInfo folderInfo)
        {
            return await _checker.FileCheckOnTarget(contentItem, folderInfo);
        }
        
        private async Task<PageUpdateResult> UpdateUploadPage(int pageId, FileContentItem contentItem, FolderInfo folderInfo)
        {
            if (contentItem.TargetPageType == InteractPageType.UploadPage)
            {
                return await _pageBuilder.UpdateUploadPage(pageId, contentItem.FullPath);
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        private async Task<PageCreateResult> BuildUploadPage(FileContentItem contentItem, FolderInfo folderInfo)
        {
            if (contentItem.TargetPageType == InteractPageType.UploadPage)
            {
                return await CreateUploadPage(contentItem, folderInfo);
            }
            else
            {
                return await CreateBuildPage(contentItem, folderInfo);
            }
        }

        private async Task<PageCreateResult> CreateBuildPage(FileContentItem contentItem, FolderInfo folderInfo)
        {
            var pci = new PageCreateInfo();
            pci.FileLocation = contentItem.FullPath;
            pci.Title = contentItem.Title;
            pci.TopSectionIds = new[] { folderInfo.SectionId };
            pci.CategoryIds = new[] { folderInfo.CategoryId };
            pci.AuthorId = _appConfig.DefaultAuthorId;
            pci.AssetId = _appConfig.DefaultAssetId;
            pci.LastUpdated = contentItem.LastUpdatedStamp;
            pci.Keywords = new[] { $"Source folder {folderInfo.FolderName}" };
            var r = await _pageBuilder.CreateBuildPage(pci);
            return r;
        }

        private async Task<PageCreateResult> CreateUploadPage(FileContentItem contentItem, FolderInfo folderInfo)
        {
            var pci = new PageCreateInfo();
            pci.FileLocation = contentItem.FullPath;
            pci.Title = contentItem.Title;
            pci.TopSectionIds = new[] { folderInfo.SectionId };
            pci.CategoryIds = new[] { folderInfo.CategoryId };
            pci.AuthorId = _appConfig.DefaultAuthorId;
            pci.AssetId = _appConfig.DefaultAssetId;
            pci.LastUpdated = contentItem.LastUpdatedStamp;
            pci.Keywords = new[] { $"Source folder {folderInfo.FolderName}" };
            var r = await _pageBuilder.CreateUploadPage(pci);
            return r;
        }
        private void LogFileAsIncompatible(FileContentItem contentItem, FolderInfo folderInfo)
        {
            var msg = $"File {contentItem.FileName} - type {contentItem.Extension} not compatible";
            _logger.LogWrite(msg);
            _logger.ActionFileNotCompatible(contentItem.FileName, folderInfo.FolderName, folderInfo.FolderId, contentItem.FullPath, folderInfo.SectionId, folderInfo.CategoryId, msg);
        }

        private bool ValidateFileType(FileContentItem contentItem)
        {
            return contentItem.Extension switch
            {
                ".pdf" => true,
                ".html" => true,
                ".doc" => true,
                ".docx" => true,
                ".xls" => true,
                ".xlsx" => true,
                ".ppt" => true,
                ".pptx" => true,
                ".txt" => true,
                ".rtf" => true,
                _ => false
            };
        }
        private void LogFolder(FolderInfo folderInfo)
        {
            _logger.LogWrite($"Checking folder {folderInfo.FolderName}");
            _logger.ActionLogFolder(folderInfo.FolderId, folderInfo.FolderName, folderInfo.SectionId, folderInfo.CategoryId, folderInfo.FolderFullPath);
        }
        private void LogProcessFilesInFolder(FolderInfo folderInfo, int count)
        {
            _logger.LogWrite($"Folder contains {count} files");
            _logger.ActionLogProcessFilesInFolder(folderInfo.FolderId, folderInfo.FolderName, count);
        }
        private void LogFileExists(FileContentItem contentItem, FolderInfo folderInfo)
        {
            _logger.ActionFileFoundInTarget(contentItem.FileName, folderInfo.FolderName, folderInfo.FolderId,
                contentItem.FullPath);
        }
        internal async Task MarkPageAsCreated(FileContentItem contentItem, FolderInfo folderInfo, PageCreateResult pageCreateResult)
        {
            _logger.LogWrite($"Page {pageCreateResult.PageId} created - {contentItem.Title} ");
            await _dbMetadata.ExecuteAsync(" Core.FileImportTrackAdd @FileName,@FolderName,@FolderId,@PageId,@FileSize,@ProcessingTime,@SectionId,@CategoryId,@SourcePath,@FileLastWriteStamp", new
            {
                contentItem.FileName,
                folderInfo.FolderName,
                folderInfo.FolderId,
                pageCreateResult.PageId,
                FileSize = contentItem.Size,
                ProcessingTime = pageCreateResult.ProcessTime.TotalSeconds,
                folderInfo.SectionId,
                folderInfo.CategoryId,
                SourcePath = contentItem.FullPath,
                FileLastWriteStamp = contentItem.LastUpdatedStamp
            });
        }
        private void MarkPageCreationFailed(FileContentItem contentItem, FolderInfo folderInfo, PageCreateResult pageResult)
        {
            _logger.LogWrite($"Page creation failed: {pageResult.FailMessage}; {contentItem.FileName} ", LogEventType.Warning);
            _logger.ActionPageCreationFailed(folderInfo.FolderId, folderInfo.FolderName, contentItem.FileName, pageResult.FailMessage);
        }
        private void LogProcessedFilesInFolder(FolderInfo folderInfo, in int completed, in int failed, in int existed, int eligible, in int updated)
        {
            var msg = $"Folder processing completed {folderInfo.FolderName}; {eligible} eligible, {existed} existing, {completed} completed, {updated} updated, {failed} failed,";
            _logger.LogWrite(msg, LogEventType.Information);
            _logger.ActionLogProcessFilesInFolderCompleted(folderInfo.FolderId, existed, completed, failed, eligible, msg, updated);
        }
        private async Task MarkPageAsUpdated(FileContentItem contentItem, FolderInfo folderInfo, PageUpdateResult pageUpdateResult, int importTrackId)
        {
            _logger.LogWrite($"Page {pageUpdateResult.PageId} updated - {contentItem.Title} ");
            await _dbMetadata.ExecuteAsync(
                "UPDATE Core.FileImportTrack SET FileSize=@FileSize, SourceFilePath=@SourcePath, ProcessingTimeSeconds=@TimeTook, UpdatedStamp=sysdatetime(), FileLastWriteStamp=@FileWriteStamp WHERE FileImportTrackId=@Id", 
                new
                {
                    SourcePath = contentItem.FullPath, TimeTook = pageUpdateResult.ProcessTime.TotalSeconds, Id=importTrackId,
                    FileWriteStamp = contentItem.LastUpdatedStamp, FileSize = contentItem.Size
                });
        }
        private async Task MarkPageUpdateFailed(FileContentItem contentItem, FolderInfo folderInfo, PageUpdateResult updatedResult)
        {
            _logger.LogWrite($"Page creation failed: {updatedResult.FailMessage}; {contentItem.FileName} ", LogEventType.Warning);
            _logger.ActionPageUpdateFailed(folderInfo.FolderId, folderInfo.FolderName, contentItem.FileName, updatedResult.FailMessage, folderInfo.SectionId, folderInfo.CategoryId);
        }
    }

  
}
