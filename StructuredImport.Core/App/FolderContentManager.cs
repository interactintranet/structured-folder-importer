﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using StructuredImport.Core.Model;

namespace StructuredImport.Core.App
{
    internal class FolderContentManager
    {
        //public List<FileContentItem> FileList { get ; private set; } = new List<FileContentItem>();
        private List<FileContentItem> _fileList = new List<FileContentItem>();

        public string CurrentFolder { get; }

        public FolderContentManager(string folderName)
        {
            CurrentFolder = folderName;
            LoadFolder();
        }

        private void LoadFolder()
        {
            
            ClearFileTable();
            var fls = Directory.EnumerateFiles(CurrentFolder).ToList();
            foreach (var fl in fls.OrderBy(n=>n))
            {
                AddFile(fl);
            }
        }

        private void AddFile(string filePathAndName)
        {
            if (IsFolderMetadataFile(filePathAndName))
            {
                LoadFolderMetadata(filePathAndName);
                return;
            }
            var fci = new FileContentItem();
            var fileInfo = new FileInfo(filePathAndName);
            fci.Size = fileInfo.Length;
            fci.Extension = fileInfo.Extension;
            fci.RawName = fileInfo.Name;
            fci.FileName = fileInfo.Name;
            fci.Title = Path.GetFileNameWithoutExtension(filePathAndName);
            fci.FullPath = filePathAndName;
            fci.LastUpdatedStamp = fileInfo.LastWriteTime;

            if ( fci.Extension == ".meta")
            {
                fci.IsMetadata = true;
                fci.FileName = fci.RawName.Replace(".meta", "");
            }
            
            _fileList.Add(fci);
        }

        private void LoadFolderMetadata(string fileName)
        {
            var c = File.ReadAllText(fileName);
            var mapInfo = JsonSerializer.Deserialize<FolderJsonMetadata>(c);
            SectionId = mapInfo.sectionId;
            CategoryId = mapInfo.categoryId;
            DefaultPageTypeName = mapInfo.defaultPageType;
            BuildPageTypeList = mapInfo.buildPageTypes.ToList();
            UploadPageTypeList = mapInfo.uploadPageTypes.ToList();
            FileExistsAction = mapInfo.fileAlreadyExists.ToLower() switch
            {
                "skip" => FileExistsBehaviour.Skip,
                "overwrite" => FileExistsBehaviour.Overwrite,
                _ => FileExistsBehaviour.Skip
            };
        }

        public  FileExistsBehaviour FileExistsAction { get; set; }
        public List<string> UploadPageTypeList { get; set; }

        public List<string> BuildPageTypeList { get; set; }

        public string DefaultPageTypeName { get; set; }

        public int CategoryId { get; set; }

        private bool IsFolderMetadataFile(string fileName)
        {
            return Path.GetFileName(fileName).ToLower() == "_foldermap.json";
        }

        private void ClearFileTable()
        {
            _fileList = new List<FileContentItem>();
        }

        public bool HasFiles => ContentCount > 0;

        public int SectionId { get; set; }

        public int ContentCount
        {
            get
            {
                return _fileList.Count(r => !r.IsMetadata);
            }
        }

        public IEnumerable<FileContentItem> FileList  => _fileList.Where(f=> !f.IsMetadata);
    }

    
}
