﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using StructuredImport.Core.Model;
using StructuredImport.Core.Model.AppPage;
using StructuredImport.Core.Model.InteractPage;
using StructuredImport.Core.Model.InteractPage.PageDownload;

namespace StructuredImport.Core.App
{
    public class PageBuilder
    {
        private AppConfig config;
        private TokenHolder _tokenHolder;

        public PageBuilder(AppConfig config)
        {
            this.config = config;
            _tokenHolder = new TokenHolder(config);
        }

        internal async Task<PageCreateResult> CreateUploadPage(PageCreateInfo buildInfo)
        {
            //_tokenHolder ??= new TokenHolder(config);
            var apiToken = _tokenHolder.CurrentToken;

            var startTime = DateTime.Now;

            //create page payload
            var interactPage = BuildPagePrototype();

            //Upload the file and prep metadata
            var storageResult = await UploadFileToFileStorage(buildInfo.FileLocation);

            if (storageResult.Failed)
            {
                return new PageCreateResult
                {
                    FailMessage = storageResult.Message, PageWasCreated = false
                };
            }

            interactPage.PrepareAttachmentMetadata(storageResult.UploadAttachment, buildInfo);

            MapProperties(interactPage, buildInfo);
            interactPage.Page.ContentType = "upload";

            //Create the page with the uploaded file data
            var result = await CreatePageFromPayload(interactPage, apiToken);
            result.ProcessTime = DateTime.Now.Subtract(startTime);
            return result;
        }

        private void MapProperties(PageUpload interactPage, PageCreateInfo buildInfo)
        {
            interactPage.Page.Title = buildInfo.Title;
            interactPage.Page.AuthorId = buildInfo.AuthorId;
            interactPage.Page.TopSectionIds = buildInfo.TopSectionIds;
            interactPage.Page.CategoryIds = buildInfo.CategoryIds;
            interactPage.Page.PubStartDate = buildInfo.LastUpdated;
            interactPage.Page.PubEndDate = buildInfo.LastUpdated.AddMonths(config.PageLifetimeInMonths);
            interactPage.Page.ReviewDate = interactPage.Page.PubEndDate.AddMonths(config.PageReviewOffsetInMonths);
            if (buildInfo.AssetId.HasValue)
            {
                interactPage.Page.AssetId = buildInfo.AssetId.Value;
            }

            if (!string.IsNullOrWhiteSpace(buildInfo.Summary))
            {
                interactPage.Page.Summary = buildInfo.Summary;
            }

            AddKeyword(interactPage, buildInfo.Keywords);
        }

        internal async Task<PageCreateResult> CreatePageFromPayload(PageUpload interactPage, string apiToken)
        {
            var payload = JsonConvert.SerializeObject(interactPage);

            var urlToHit = $"{config.InteractApiUrl}/api/page/composer";
            var result = await InteractApiHandler.Post(urlToHit, payload, config.InteractTenantId, apiToken);

            var pageCreated = result.Header.IsSuccessStatusCode;
            var failMessage = pageCreated ? string.Empty : result.Body;

            var newPageId = pageCreated ? GetPageId(result.Body) : -1;

            return new PageCreateResult
            {
                PageId = newPageId, PageWasCreated = pageCreated, FailMessage = failMessage
            };
        }
        /// <summary>
        /// Upload via API has a file size limit which seems to be imposed irrespective of the settings in AppVars.
        /// There is an alternative: we can copy a file onto the file share rather then upload it. This will only work if the share is mapped, so
        /// the machine running this code can see it, therefore it has to run inside the VPC
        /// (there is a third way, by using a chunking method that the API supports but that has not been investigated.)
        /// </summary>
        /// <param name="fileLocation"></param>
        /// <returns></returns>
        internal async Task<UploadFileToFileStorageResult> UploadFileToFileStorage(string fileLocation)
        {
            switch (config.FileUploadMode)
            {
                case FileUploadMode.UseApi:
                    return await UploadFileToFileStorageViaApi(fileLocation);
                case FileUploadMode.UseFileCopy:
                    return await UploadFileToFileStorageViaCopy(fileLocation);
                default:
                    return await UploadFileToFileStorageViaApi(fileLocation);
            }
        }

        internal async Task<UploadFileToFileStorageResult> UploadFileToFileStorageViaCopy(string fileLocation)
        {
            var fileGuid = Guid.NewGuid().ToString();
            var fileName = Path.GetFileName(fileLocation);
            var destinationPath = Path.Combine(config.InteractServerResourcesFolder, fileGuid);
            var content = File.OpenRead(fileLocation);
            var destination = File.OpenWrite(destinationPath);
            await content.CopyToAsync(destination);
            await destination.FlushAsync();
            destination.Close();
            content.Close();
            return new UploadFileToFileStorageResult
            {
                UploadAttachment = new FileUploadAttachment
                {
                    Extension = Path.GetExtension(fileLocation),
                    FileGuid = fileGuid,
                    Title = fileName,
                    Path =
                        $"/Utilities/Uploads/Handler/Uploader.ashx?area=composer&filename={fileName}&fileguid={fileGuid}"
                }
            };
        }

        internal async Task<UploadFileToFileStorageResult> UploadFileToFileStorageViaApi(string fileLocation)
        {

            var content = await File.ReadAllBytesAsync(fileLocation);
            var fileName = Path.GetFileName(fileLocation);
            var url = $"{config.InteractApiUrl}/api/asset/upload/composer";
            var uploadResponse = await InteractApiHandler.PostUpload(url, content, fileName, config.InteractTenantId,
                _tokenHolder.CurrentToken);

            if (!uploadResponse.Header.IsSuccessStatusCode)
            {
                return new UploadFileToFileStorageResult
                {
                    Failed = true, Message = uploadResponse.Body
                };
            }

            var bodyResponse = JsonConvert.DeserializeObject<UploadResponseBody>(uploadResponse.Body);

            return new UploadFileToFileStorageResult
            {
                UploadAttachment = bodyResponse.FileInfo.FileUploadAttachment
            };
        }

        internal PageUpload BuildPagePrototype()
        {
            var pagePayload = new PageUpload();
            pagePayload.Transition.State = "published";
            pagePayload.Transition.Message = "Uploaded by migration process";
            pagePayload.Page.ContentType = "html";
            pagePayload.Page.PublishAsId = null;
            pagePayload.Page.Title = "Uploaded";
            pagePayload.Page.Summary = "Uploaded by migration process";
            pagePayload.Page.Content.Html = "<p>Content here</p>";
            pagePayload.Page.Features.AllowComments = true;
            pagePayload.Page.Features.DefaultToFullWidth = false;
            pagePayload.Page.Features.IsKeyPage = false;
            pagePayload.Page.Features.Recommends.MaxContentAge = 7;
            pagePayload.Page.Features.Recommends.Show = true;
            pagePayload.Page.PubStartDate = DateTime.Now;
            pagePayload.Page.PubEndDate = DateTime.Now.AddYears(5);
            pagePayload.Page.ReviewDate = DateTime.Now.AddYears(2);
            pagePayload.Page.Keywords = new[] {"Migrated"};
            pagePayload.Page.TagIds = new int[] { };
            return pagePayload;
        }

        private int GetPageId(string result)
        {
            if (!result.Contains("ContentId")) return 0;
            dynamic resultPack = JsonConvert.DeserializeObject<InteractPageApiResponse>(result);
            return resultPack.ContentId;
        }

        private void AddKeyword(PageUpload page, params string[] newKeywords)
        {
            if (newKeywords == null) return;
            if (!newKeywords.Any()) return;
            var a = page.Page.Keywords.ToList();
            a.AddRange(newKeywords);
            page.Page.Keywords = a.ToArray();
        }

        public async Task<PageDownload> FetchPage(int pageId)
        {
            var url = $"{config.InteractApiUrl}/api/page/{pageId}/composer/latest";
            var data = await InteractApiHandler.Get(url, config.InteractTenantId, _tokenHolder.CurrentToken);
            if (data.Header.IsSuccessStatusCode)
            {
                var pu = JsonConvert.DeserializeObject<PageDownload>(data.Body);
                return pu;
            }

            return new PageDownload();
        }

        internal async Task<string> FetchPagePayload(int pageId)
        {
            var url = $"{config.InteractApiUrl}/api/page/{pageId}/composer/latest";
            var data = await InteractApiHandler.Get(url, config.InteractTenantId, _tokenHolder.CurrentToken);
            return data.Header.IsSuccessStatusCode ? data.Body : string.Empty;
        }
        internal async Task<PageUpdateResult> UpdateUploadPage(int pageId, string fileLocation)
        {
            _tokenHolder ??= new TokenHolder(config);
            var apiToken = _tokenHolder.CurrentToken;

            var startTime = DateTime.Now;

            //obtain current page payload and transform to a state ready for updating
            var interactPageExisting = await FetchPage(pageId);
            var interactPage = Map(interactPageExisting);

            //Upload the file and prep metadata
            var storageResult = await UploadFileToFileStorage(fileLocation);

            if (storageResult.Failed)
            {
                return new PageUpdateResult
                {
                    FailMessage = storageResult.Message,
                    PageWasUpdated = false
                };
            }
            var buildInfo = new PageCreateInfo
            {
                FileLocation = fileLocation,
                GoStraightToTarget = interactPageExisting.Content.Upload.GoStraightToTarget,
                DisableDownload = interactPageExisting.Content.Upload.DisableDownload,
                Target = interactPageExisting.Content.Upload.Target
            };

            interactPage.PrepareAttachmentMetadata(storageResult.UploadAttachment, buildInfo);

            interactPage.Transition.State = "published";
            interactPage.Transition.Message = "Update to file";
            
            var result = await UpdatePageFromPayload(interactPage, pageId, apiToken);

            result.ProcessTime = DateTime.Now.Subtract(startTime);
            return result;
        }

        internal async Task<PageUpdateResult> UpdatePageFromPayload(PageUpload interactPage, int pageId, string apiToken)
        {
            var payload = JsonConvert.SerializeObject(interactPage);

            var urlToHit = $"{config.InteractApiUrl}/api/page/{pageId}/composer";
            var result = await InteractApiHandler.Put(urlToHit, payload, config.InteractTenantId, apiToken);

            var pageCreated = result.Header.IsSuccessStatusCode;
            var failMessage = pageCreated ? string.Empty : result.Body;

            var newPageId = pageCreated ? GetPageId(result.Body) : -1;

            return new PageUpdateResult()
            {
                PageId = newPageId,
                PageWasUpdated = pageCreated,
                FailMessage = failMessage
            };
        }

        public static PageUpload Map(PageDownload pageDownload)
        {
            var upload = new PageUpload();
            upload.Page.PubEndDate = pageDownload.PubEndDate;
            upload.Page.PubStartDate = pageDownload.PubStartDate;
            upload.Page.ReviewDate = pageDownload.ReviewDate;
            upload.Page.AssetId = pageDownload.AssetId;
            upload.Page.Title = pageDownload.Title;
            upload.Page.AuthorId = pageDownload.Author.Id;
            upload.Page.CategoryIds = pageDownload.Categories.Select(c => c.Id).ToArray();
            upload.Page.ContentType = pageDownload.ContentType;
            upload.Page.Content.Html = pageDownload.Content.Html;
            upload.Page.Features.AllowComments = pageDownload.Features.AllowComments;
            upload.Page.Features.DefaultToFullWidth = pageDownload.Features.DefaultToFullWidth;
            upload.Page.Features.IsKeyPage = pageDownload.Features.IsKeyPage;
            if (pageDownload.Features.Recommends.MaxContentAge.HasValue)
            {
                upload.Page.Features.Recommends.MaxContentAge = pageDownload.Features.Recommends.MaxContentAge.Value;
            }
            upload.Page.Features.Recommends.Show = pageDownload.Features.Recommends.Show;
            upload.Page.PageAttachments = pageDownload.PageAttachments;
            upload.Page.Keywords = pageDownload.Keywords.Select(k => k.Value).ToArray();
            upload.Page.Summary = pageDownload.Summary;
            upload.Page.TagIds = pageDownload.Tags.Select(t => t.Id).ToArray();
            upload.Page.TopSectionIds = pageDownload.TopSectionIds;
            return upload;

        }

        internal async Task<PageCreateResult> CreateBuildPage(PageCreateInfo buildInfo)
        {
            var apiToken = _tokenHolder.CurrentToken;

            var startTime = DateTime.Now;

            var interactPage = BuildPagePrototype();

            var fileContent = await File.ReadAllTextAsync(buildInfo.FileLocation);
           
            MapProperties(interactPage, buildInfo);
            interactPage.Page.ContentType = "html";
            interactPage.Page.Content.Html = fileContent;

            var result = await CreatePageFromPayload(interactPage, apiToken);
            result.ProcessTime = DateTime.Now.Subtract(startTime);
            return result;
        }
    }

    internal class UploadFileToFileStorageResult
    {
        public FileUploadAttachment UploadAttachment { get; set; }
        public bool Failed { get; set; }
        public string Message { get; set; }
    }

}
