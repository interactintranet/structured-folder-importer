﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using StructuredImport.Core.Model;

namespace StructuredImport.Core.App
{
    public class TokenHolder
    {
        private AppConfig _config;
        private string _fullToken;
        private string _tokenPart;
        private string _expiry;
        private DateTime _expiryDateTime = DateTime.Now;
        public DateTime TokenExpiry => _expiryDateTime;
        public TokenHolder(AppConfig config)
        {
            _config = config;
        }

        public string CurrentToken
        {
            get
            {
                if (!TokenNeedsRefreshing) return _tokenPart;
                _fullToken = GetFullToken();
                GetTokenPart();

                return _tokenPart;
            }
        }

        private void GetTokenPart()
        {
            dynamic obj = JsonConvert.DeserializeObject(_fullToken);

            _tokenPart = obj.access_token;
            _expiry = obj.expires;
            _expiryDateTime = DateTime.Parse(_expiry, new CultureInfo("en-US"));
        }

        internal bool TokenNeedsRefreshing
        {
            get => (_expiryDateTime.Subtract(DateTime.Now).TotalMinutes < 6);
        }



        private string GetFullToken()
        {
            var fullUrl = $"{_config.InteractApiUrl}/token";
            Console.WriteLine($"Getting Interact token as user {_config.InteractUserName}....");
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fullUrl);
            request.Headers.Add("X-Tenant", _config.InteractTenantId);
            request.Method = "POST";

            string body = $"grant_type=password&username={_config.InteractUserName}&password={_config.InteractPassword}";

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(body);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/x-www-form-urlencoded";

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }

            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    return reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    string errorText = reader.ReadToEnd();
                }
                throw;
            }
        }
    }
}
