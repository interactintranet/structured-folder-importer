﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace StructuredImport.Core.App
{
    internal class InteractApiHandler
    {
        public static async Task<PostResponse> Post(string url, string payload, string tenant, string accessToken)
        {

            using (var hr = new HttpClient())
            {
                hr.DefaultRequestHeaders.Add("X-Tenant", tenant.ToString());
                hr.DefaultRequestHeaders.Add("Authorization", $"Bearer {accessToken}");
                var content = new StringContent(payload, Encoding.UTF8, "application/json");

                var response = await hr.PostAsync(url, content);

                var cc = await response.Content.ReadAsStringAsync();
                return new PostResponse
                {
                    Header = response,
                    Body = cc
                };
            }
        }

        public static async Task<GetResponse> Get(string url, string tenant, string accessToken)
        {
            using (var hr = new HttpClient())
            {
                hr.DefaultRequestHeaders.Add("X-Tenant", tenant.ToString());
                hr.DefaultRequestHeaders.Add("Authorization", $"Bearer {accessToken}");
                
                var response = await hr.GetAsync(url);
                var content = await response.Content.ReadAsStringAsync();
                return new GetResponse
                {
                    Header = response,
                    Body = content
                };
            }
        }

        public static async Task<UploadResponse> PostUpload(string url, byte[] contentBlock, string fileName, string tenant, string accessToken)
        {
            var form = new MultipartFormDataContent();
            form.Add(new ByteArrayContent(contentBlock, 0, contentBlock.Length), "document", fileName);
            using var hr = new HttpClient();
            hr.DefaultRequestHeaders.Add("X-Tenant", tenant.ToString());
            hr.DefaultRequestHeaders.Add("Authorization", $"Bearer {accessToken}");
            var response = await hr.PostAsync(url, form);
            var uploadResponse = new UploadResponse {Header = response};

            if (response.IsSuccessStatusCode)
            {
                var contents = await response.Content.ReadAsStringAsync();
                uploadResponse.Body = contents;
            }
            else
            {
                uploadResponse.Body = response.ReasonPhrase;
            }

            return uploadResponse;

            //if (!response.IsSuccessStatusCode)
            //{
            //    return new UploadResponse
            //    {
            //        Header = response,
            //        Body = response.ReasonPhrase
            //    };
            //}
            //var body = await response.Content.ReadAsStringAsync();
            ////var assetDetails = JsonConvert.DeserializeObject<dynamic>(body);
            //return new UploadResponse
            //{
            //    Header = response, Body = body //, AssetId = assetDetails.assetId
            //};
        }
        public static async Task<Tuple<HttpResponseMessage, string>> Delete(string url, string tenant, string accessToken)
        {
            using var hr = new HttpClient();
            hr.DefaultRequestHeaders.Add("X-Tenant", tenant);
            hr.DefaultRequestHeaders.Add("Authorization", $"Bearer {accessToken}");

            var response = await hr.DeleteAsync(url);

            var cc = await response.Content.ReadAsStringAsync();
            return new Tuple<HttpResponseMessage, string>(response, cc);
        }

        public static async Task<PutResponse> Put(string url, string payload, string tenant, string accessToken)
        {
            using (var hr = new HttpClient())
            {
                hr.DefaultRequestHeaders.Add("X-Tenant", tenant);
                hr.DefaultRequestHeaders.Add("Authorization", $"Bearer {accessToken}");
                var content = new StringContent(payload, Encoding.UTF8, "application/json");

                var response = await hr.PutAsync(url, content);

                var cc = await response.Content.ReadAsStringAsync();
                return new PutResponse
                {
                    Header = response,
                    Body = cc
                };
            }
        }
    }


    internal class ResponseBase
    {
        public HttpResponseMessage Header { get; set; }
        public string Body { get; set; }
    }
    internal  class PostResponse : ResponseBase{}
    internal class GetResponse : ResponseBase{}
    internal class UploadResponse : ResponseBase
    {
        public int AssetId { get; set; }
    }
    internal class PutResponse: ResponseBase{}

   
}
