﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StructuredImport.Core.Model;

namespace StructuredImport.Core.App
{
    public class ImporterBase
    {
        //private AppConfig _appConfig;
        //public ImporterBase(AppConfig appConfig)
        //{
        //    _appConfig = appConfig;
        //}

        [DebuggerStepThrough]
        protected void LogWrite(string message, LogEventType type = LogEventType.Information)
        {
            EchoConsole(message, type);
            WriteDbLog(message, type);
        }

        [DebuggerStepThrough]
        protected void LogWrite(Exception e, string context)
        {
            EchoConsole(e.ToString(), ConsoleColor.Red);
            WriteDbLog(e.ToString(), LogEventType.Exception);
            WriteDbLog(context, LogEventType.Exception);
        }

        [DebuggerStepThrough]
        protected void LogWrite(Exception e)
        {
            Console.WriteLine(e.Message);
            WriteDbLog(e.ToString(), LogEventType.Exception);
        }

        [DebuggerStepThrough]
        protected void WriteDbLog(string message, LogEventType level)
        {
            //if (!config.LogToDatabase) return;

            //try
            //{
            //    dbMigration.Execute($"EXEC {_schema}.LogWrite @SourceId, @Message, @Level, @CorrelationId", new
            //    {
            //        SourceId = _operation,
            //        Message = message,
            //        Level = level,
            //        CorrelationId = _correlationId
            //    });
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e);

            //}
        }
        [DebuggerStepThrough]
        protected void EchoConsole(string message, ConsoleColor colour)
        {
            Console.ForegroundColor = colour;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }


        [DebuggerStepThrough]
        protected void EchoConsole(string message, LogEventType type = LogEventType.Information)
        {
            var col = type switch
            {
                LogEventType.Error => ConsoleColor.Red,
                LogEventType.Exception => ConsoleColor.Red,
                LogEventType.Warning => ConsoleColor.Yellow,
                LogEventType.Progress => ConsoleColor.Cyan,
                LogEventType.Commencement => ConsoleColor.Magenta,
                LogEventType.Termination => ConsoleColor.Green,
                LogEventType.Trace => ConsoleColor.DarkCyan,
                LogEventType.Notable => ConsoleColor.DarkYellow,
                _ => ConsoleColor.White
            };

            EchoConsole(message, col);
        }
    }
}
