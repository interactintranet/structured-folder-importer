﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StructuredImport.Core.Model;

namespace StructuredImport.Core.App
{
    public class ContentImporter : ImporterBase
    {
        //public async Task ImportFolders(string startingFolder, bool recursively)
        //{
        //    var folderMgr = new FolderManager(startingFolder, recursively);
        //}

        public async Task<ImportFolderResult> ImportContentFromFolder(string folderName)
        {
            var folderContent = new FolderContentManager(folderName);
            if (folderContent.HasFiles == false)
            {
                LogWrite($@"No files in folder {folderName}", LogEventType.Warning);
                return new ImportFolderResult {FolderContentCount = 0};
            }

            var contentResult = new ImportFolderResult();
            contentResult.FolderContentCount = folderContent.ContentCount;

            foreach (var contentItem in folderContent.FileList)
            {
               var itemResult = await ImportContentItem(contentItem, folderContent);
            }


            return contentResult;
        }

        internal async Task<ImportContentResult> ImportContentItem(FileContentItem contentItem, FolderContentManager folderContent)
        {
            //var file = contentItem.
            var categoryId = folderContent.CategoryId;
            var sectionId = folderContent.SectionId;

            return new ImportContentResult();
        }
    }



   
}
