﻿namespace StructuredImport.Core.Support
{
    enum  EventTypes
    {
        LogFolder =1,
        LogProcessFilesInFolder,
        PageCreationFailed,
        FileAlreadyExistsOnTarget,
        FolderProcessingCompleted,
        PageUpdateFailed,
        FileNotCompatibleExtension
    }
}
