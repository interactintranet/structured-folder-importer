﻿using StructuredImport.Core.Model;
using System;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using StructuredImport.Core.App;

namespace StructuredImport.Core.Support
{
    /// <summary>
    /// Used during testing to tidy up folders. Not currently used in the app
    /// </summary>
    public class DeleteProcessor 
    {
        private AppConfig _appConfig;
        private SqlConnection _dbMetadata;
        private TokenHolder _tokenHolder;
        private string _correlationId;
        private DbLogger _logger;

        public DeleteProcessor(AppConfig config)
        {
            _appConfig = config;
            _dbMetadata = new SqlConnection(_appConfig.MetadataDatabaseConnectionString);
            _tokenHolder = new TokenHolder(_appConfig);
            _correlationId = Guid.NewGuid().ToString().Substring(0, 8);
            _logger = new DbLogger(_appConfig, _dbMetadata, _correlationId);
        }

        public async Task ProcessDeleteQueue(bool skipPreviouslyAttempted = false)
        {
            //Process list of pages to be deleted
            var sql = "SELECT * FROM Core.DeleteQueue WHERE DeletedStamp IS NULL ";
            if (skipPreviouslyAttempted)
            {
                sql += "AND AttemptedStamp IS NULL ";
            }
            sql += "ORDER BY PageId";

            var queue = _dbMetadata.Query<DeleteQueueDto>(sql).ToList();
            var token = _tokenHolder.CurrentToken;

            _logger.LogWrite($"Delete Queue has {queue.Count} items", LogEventType.Commencement);
            foreach (var page  in queue)
            {
                _dbMetadata.Execute($"UPDATE Core.DeleteQueue SET AttemptedStamp=SysDateTime() WHERE PageId={page.PageId}");
                var urlToHit = $"{_appConfig.InteractApiUrl}api/page/{page.PageId}";
                var (response, failureMessage) = await InteractApiHandler.Delete(urlToHit, _appConfig.InteractTenantId, token);
                if (response.IsSuccessStatusCode)
                {
                    _logger.LogWrite($"Page {page.PageId} deleted");
                    _dbMetadata.Execute($"UPDATE Core.DeleteQueue SET DeletedStamp=SysDateTime() WHERE PageId={page.PageId}");
                }
                else
                {
                    _logger.LogWrite($"Delete failed for page {page.PageId}  {failureMessage}", LogEventType.Warning);
                    _dbMetadata.Execute("UPDATE Core.DeleteQueue SET FailureMessage=@Message WHERE PageId=@PageId", new
                    {
                        Message  = failureMessage.PadRight(500).Substring(0,500).Trim(),  page.PageId
                    });
                }
            }
            _logger.LogWrite($"Delete Queue processing completed", LogEventType.Termination);
        }
        

        internal class DeleteQueueDto
        {
            public int PageId { get; set; }
        }
    }
}
