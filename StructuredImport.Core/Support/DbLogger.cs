﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using Dapper;
using StructuredImport.Core.Model;

namespace StructuredImport.Core.Support
{
    class DbLogger
    {
        private AppConfig _appConfig;
        private readonly string _correlationId;
        public string CorrelationId => _correlationId;
        public SqlConnection DbConnection { set; get; }
        public DbLogger(AppConfig appConfig, SqlConnection dbConnection = null, string correlationId ="")
        {
            _appConfig = appConfig;
            _correlationId = correlationId;
            DbConnection = dbConnection ?? new SqlConnection(_appConfig.MetadataDatabaseConnectionString);

        }

        [DebuggerStepThrough]
        internal void LogWrite(string message, LogEventType type = LogEventType.Information)
        {
            EchoConsole(message, type);
            WriteDbLog(message, type);
        }

        [DebuggerStepThrough]
        internal void LogWrite(Exception e, string context)
        {
            EchoConsole(e.ToString(), ConsoleColor.Red);
            WriteDbLog(e.ToString(), LogEventType.Exception);
            WriteDbLog(context, LogEventType.Exception);
        }

        [DebuggerStepThrough]
        internal void LogWrite(Exception e)
        {
            Console.WriteLine(e.Message);
            WriteDbLog(e.ToString(), LogEventType.Exception);
        }

       
        [DebuggerStepThrough]
        internal void EchoConsole(string message, ConsoleColor colour)
        {
            Console.ForegroundColor = colour;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }


        [DebuggerStepThrough]
        internal void EchoConsole(string message, LogEventType type = LogEventType.Information)
        {
            var col = type switch
            {
                LogEventType.Error => ConsoleColor.Red,
                LogEventType.Exception => ConsoleColor.Red,
                LogEventType.Warning => ConsoleColor.Yellow,
                LogEventType.Progress => ConsoleColor.Cyan,
                LogEventType.Commencement => ConsoleColor.Magenta,
                LogEventType.Termination => ConsoleColor.Green,
                LogEventType.Trace => ConsoleColor.DarkCyan,
                LogEventType.Notable => ConsoleColor.DarkYellow,
                _ => ConsoleColor.White
            };

            EchoConsole(message, col);
        }
        [DebuggerStepThrough]
        internal void WriteDbLog(string message, LogEventType level)
        {
            //This is not currently being used in StructuredImport

            if (!_appConfig.LogToDatabase) return;

            try
            {
                DbConnection.Execute($"EXEC Core.LogWrite @SourceId, @Message, @Level, @CorrelationId", new
                {
                    SourceId = 0,
                    Message = message,
                    Level = level,
                    CorrelationId = CorrelationId
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

            }
        }
        public void ActionLogFolder(in int folderId, string folderName, in int sectionId, in int categoryId, string folderPath)
        {
            DbConnection.Execute(
                "EXEC Core.EventWrite @EventType, @FolderId, @FolderName, @FileName, @FolderPath, @SectionId, @CategoryId, @EventMessage, @ItemCount, @CorrelationId, @Scalar1, @Scalar2, @Scalar3, @Scalar4",
            new {
                EventType = EventTypes.LogFolder, FolderId  = folderId, FolderName = folderName, FileName=(string?)null, FolderPath = folderPath, sectionId, categoryId, ItemCount = (int?)null,
                EventMessage="", CorrelationId, Scalar1 =(int?)null, Scalar2 = (int?)null, Scalar3 = (int?)null, Scalar4=(int?)null
            });
        }

        public void ActionLogProcessFilesInFolder(in int folderId, in string folderName, in int count)
        {
            DbConnection.Execute(
                "EXEC Core.EventWrite @EventType, @FolderId, @FolderName, @FileName, @FolderPath, @SectionId, @CategoryId, @EventMessage, @ItemCount, @CorrelationId, @Scalar1, @Scalar2, @Scalar3, @Scalar4",
                new
                {
                    EventType = EventTypes.LogProcessFilesInFolder,
                    FolderId = folderId,
                    FolderName = folderName,
                    FileName = (string?)null,
                    FolderPath = (string?)null,
                    SectionId = (int?)null,
                    CategoryId = (int?)null,
                    ItemCount = count,
                    EventMessage =  $"Folder contains {count} files",
                    CorrelationId,
                    Scalar1 = (int?)null,
                    Scalar2 = (int?)null,
                    Scalar3 = (int?)null,
                    Scalar4 = (int?)null

                });
        }

        public void ActionFileFoundInTarget(string? fileName, string folderName, in int folderId, string fullPath)
        {
            DbConnection.Execute(
                "EXEC Core.EventWrite @EventType, @FolderId, @FolderName, @FileName, @FolderPath, @SectionId, @CategoryId, @EventMessage, @ItemCount, @CorrelationId, @Scalar1, @Scalar2, @Scalar3, @Scalar4",
                new
                {
                    EventType = EventTypes.FileAlreadyExistsOnTarget,
                    FolderId = folderId,
                    FolderName = folderName,
                    FileName = fileName,
                    FolderPath = fullPath,
                    SectionId = (int?)null,
                    CategoryId = (int?)null,
                    ItemCount = (int?)null,
                    EventMessage ="File previously processed",
                    CorrelationId,
                    Scalar1 = (int?)null,
                    Scalar2 = (int?)null,
                    Scalar3 = (int?)null,
                    Scalar4 = (int?)null
                });
        }

        public void ActionPageCreationFailed(in int folderId, string folderName, string? fileName, string failMessage)
        {
            DbConnection.Execute(
                "EXEC Core.EventWrite @EventType, @FolderId, @FolderName, @FileName, @FolderPath, @SectionId, @CategoryId, @EventMessage, @ItemCount, @CorrelationId, @Scalar1, @Scalar2, @Scalar3, @Scalar4",
                new
                {
                    EventType = EventTypes.PageCreationFailed,
                    FolderId = folderId,
                    FolderName = folderName,
                    FileName = fileName,
                    FolderPath = (string?)null,
                    SectionId = (int?)null,
                    CategoryId = (int?)null,
                    ItemCount = (int?)null,
                    EventMessage =  failMessage,
                    CorrelationId,
                    Scalar1 = (int?)null,
                    Scalar2 = (int?)null,
                    Scalar3 = (int?)null,
                    Scalar4 = (int?)null
                });
        }

        public void ActionLogProcessFilesInFolderCompleted(in int folderId, in int existed, in int completed, in int failed, in int eligible, in string message, in int updated)
        {
            DbConnection.Execute(
                "EXEC Core.EventWrite @EventType, @FolderId, @FolderName, @FileName, @FolderPath, @SectionId, @CategoryId, @EventMessage, @ItemCount, @CorrelationId, @Scalar1, @Scalar2, @Scalar3, @Scalar4",
                new
                {
                    EventType = EventTypes.FolderProcessingCompleted,
                    FolderId = folderId,
                    FolderName = (string?)null,
                    FileName = (string?)null,
                    FolderPath = (string?)null,
                    SectionId = (int?)null,
                    CategoryId = (int?)null,
                    ItemCount = eligible,
                    EventMessage = message,
                    CorrelationId,
                    Scalar1 = completed,
                    Scalar2 = existed,
                    Scalar3 = failed,
                    Scalar4 = updated
                });
        }

        public void ActionPageUpdateFailed(int folderId, string folderName, string fileName, string failMessage,int sectionId, int categoryId)
        {
            DbConnection.Execute(
                "EXEC Core.EventWrite @EventType, @FolderId, @FolderName, @FileName, @FolderPath, @SectionId, @CategoryId, @EventMessage, @ItemCount, @CorrelationId, @Scalar1, @Scalar2, @Scalar3, @Scalar4",
                new
                {
                    EventType = EventTypes.PageUpdateFailed,
                    FolderId = folderId,
                    FolderName = folderName,
                    FileName = fileName,
                    FolderPath = (string?)null,
                    SectionId = sectionId,
                    CategoryId = categoryId,
                    ItemCount = (int?)null,
                    EventMessage = failMessage,
                    CorrelationId,
                    Scalar1 = (int?)null,
                    Scalar2 = (int?)null,
                    Scalar3 = (int?)null,
                    Scalar4 = (int?)null
                });
        }


        public void ActionFileNotCompatible(string fileName, string folderName, int folderId, string fullPath, int sectionId, int categoryId, string failMessage)
        {
            DbConnection.Execute(
                "EXEC Core.EventWrite @EventType, @FolderId, @FolderName, @FileName, @FolderPath, @SectionId, @CategoryId, @EventMessage, @ItemCount, @CorrelationId, @Scalar1, @Scalar2, @Scalar3, @Scalar4",
                new
                {
                    EventType = EventTypes.FileNotCompatibleExtension,
                    FolderId = folderId,
                    FolderName = folderName,
                    FileName = fileName,
                    FolderPath = fullPath,
                    SectionId = sectionId,
                    CategoryId = categoryId,
                    ItemCount = (int?)null,
                    EventMessage = failMessage,
                    CorrelationId,
                    Scalar1 = (int?)null,
                    Scalar2 = (int?)null,
                    Scalar3 = (int?)null,
                    Scalar4 = (int?)null
                });
        }
    }
}
