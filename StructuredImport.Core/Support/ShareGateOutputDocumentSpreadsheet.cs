﻿using StructuredImport.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Aspose.Cells;

namespace StructuredImport.Core.Support
{
    internal class ShareGateOutputDocumentSpreadsheet
    {
        private AppConfig config;
        private string fileLocation;
        private readonly License _licence;
        public List<SgRowData> DocumentList { get; private set; }

        public ShareGateOutputDocumentSpreadsheet()
        {
            
        }

        public ShareGateOutputDocumentSpreadsheet(AppConfig config, string fileLocation)
        {
            this.config = config;
            _licence = new License();
           // _licence.SetLicense(config.LicencePath);
            this.fileLocation = fileLocation;
        }

        public void ParseDoc()
        {
            var workbook = new Workbook(fileLocation);
            var ws = workbook.Worksheets[0];
            var sgRowList = new List<SgRowData>();
            foreach (Row row in ws.Cells.Rows)
            {
                var rowData = new SgRowData();
                rowData.ContentType = row[(int)SgColumnId.ContentType].StringValue;
                rowData.SourcePath = row[(int)SgColumnId.SourcePath].StringValue;
                rowData.DestinationPath = row[(int)SgColumnId.DestinationPath].StringValue;
                rowData.Title = row[(int)SgColumnId.Title].StringValue;
                rowData.ModifiedBy = row[(int)SgColumnId.ModifiedBy].StringValue;
                rowData.CreatedBy = row[(int)SgColumnId.CreatedBy].StringValue;
                rowData.CreatedAt = row[(int)SgColumnId.CreatedAt].StringValue;
                rowData.ModifiedAt = row[(int)SgColumnId.ModifiedAt].StringValue;
                sgRowList.Add(rowData);
            }

            DocumentList = sgRowList;
        }
    }

    internal enum SgColumnId
    {
        SourcePath=0,
        DestinationPath=1,
        ContentType=2,
        CreatedAt=3,
        Title=4,
        ModifiedAt=5,
        CreatedBy = 18,
        ModifiedBy=19
    }
    internal class SgRowData
    {
        public string SourcePath { get; set; } 
        public string DestinationPath { get; set; }
        public string ContentType { get; set; }
        public string CreatedAt {get;set;} 
        public string Title { get; set; }
        public string ModifiedAt { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
    }
}
