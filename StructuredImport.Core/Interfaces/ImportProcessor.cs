﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StructuredImport.Core.Model;

namespace StructuredImport.Core.Interfaces
{
    interface IImportProcessor
    {
        //Task<ImportProcessorResult> RunProcess();
        Task<ImportProcessorResult> RunProcess(bool dryRun = false);
    }
}
