﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructuredImport.Core.Model
{
    public class AppConfig
    {
        //public string MigrationDatabaseConnectionString { get; set; }    //Connection string to the DB storing the Jive metadata and migration settings
        //public string InteractDatabaseConnectionString { get; set; }    //Connection string to an Interact database
        public string MetadataDatabaseConnectionString { get; set; }
        public string InteractApiUrl { get; set; }                      //API endpoint for the Interact API for InteractDatabaseConnectionString
        public string InteractTenantId { get; set; }                    //Tenant Id
        public string InteractUserName { get; set; }                    //Credentials for connecting to Interact instance via API
        public string InteractPassword { get; set; }                    //Credentials for connecting to Interact instance via API
        //public int TargetSystemId { get; set; }                         //Determines which config to use
        //public string InteractServerResourcesFolder { get; set; }       //File folder on a server to copy files to
        public int DefaultAuthorId { get; set; }                        //Who to set as the author when uploading things to Interact, if we can't establish the user from Jive
        public int DefaultAssetId { get; set; }                         //What Icon to use for uploaded content
        public bool ExistenceCheckByMetadata { get; set; }
        public bool ExistenceCheckByQueryingSite { get; set; }
        public bool LogToDatabase { get; set; }
        public int PageLifetimeInMonths { get; set; } = 24;
        public int PageReviewOffsetInMonths { get; set; } = -3;
        public string InteractServerResourcesFolder { get; set; }
        public FileUploadMode FileUploadMode { get; set; } = FileUploadMode.UseApi;
        public string LicencePath { get; set; }
    }
}
