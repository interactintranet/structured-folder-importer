﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructuredImport.Core.Model
{
    public class ImportFolderResult
    {
        public int FolderContentCount { get; set; }
    }
}
