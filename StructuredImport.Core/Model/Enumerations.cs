﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructuredImport.Core.Model
{
  
    public enum LogEventType
    {
        Information = 4,
        Error = 2,
        Warning = 3,
        Progress = 5,
        Exception = 1,
        Critical = Exception,
        Commencement = 7,
        Termination = 8,
        Notable = 6,
        Trace = 20
    }

    public enum FileExistsBehaviour
    {
        Skip,
        Overwrite
    }

    public enum FileUploadMode
    {
        UseApi,
        UseFileCopy
    }

    public enum InteractPageType
    {
        BuildPage,
        UploadPage
    }
}
