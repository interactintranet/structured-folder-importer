﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructuredImport.Core.Model
{
    public class ExistenceCheckResult
    {
        public bool FileIsOnTarget { get; set; }    
        public bool MetadataRecordIsOlder { get; set; }

        public bool UpdateIsRequired => MetadataRecordIsOlder && FileIsOnTarget;
        public int PageId { get; set; }
        public int FileImportTrackId { get; set; }
    }
}
