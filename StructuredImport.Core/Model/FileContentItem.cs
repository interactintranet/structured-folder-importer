﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructuredImport.Core.Model
{
    class FileContentItem
    {
        public string Extension { get; set; }
        /// <summary>
        /// Only used for metatdatafiles
        /// </summary>
        public string RawName { get; set; }
        public string FileName { get; set; }
        public bool IsMetadata { get; set; }
        public string FullPath { get; set; }
        public long Size { get; set; }
        public string Title { get; set; }
        public DateTime LastUpdatedStamp { get; set; }

        public InteractPageType TargetPageType
        {
            get
            {
                switch (Extension)
                {
                    case ".html":
                    case ".htm":
                        return InteractPageType.BuildPage;
                    default:
                        return InteractPageType.UploadPage;
                }
            }
        }
    }
}
