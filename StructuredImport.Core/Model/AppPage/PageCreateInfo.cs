﻿using System;
using System.Reflection.Metadata;

namespace StructuredImport.Core.Model.AppPage
{
    internal class PageCreateInfo
    {
        public string FileLocation { get; set; }
        public string Title { get; set; }
        public int[] TopSectionIds { get; set; }
        public int[] CategoryIds { get; set; }
        public int AuthorId { get; set; }
        public bool GoStraightToTarget { get; set; }
        public bool DisableDownload { get; set; }
        public string Target { get; set; }
        public string Summary { get; set; }
        public string[] Keywords { get; set; }
        public int? AssetId { get; set; }
        public DateTime LastUpdated { get; set; }
    }

    internal static class PageCreateInfoExtensions
    {
        public static int GetAssetIdFromDocType(this PageCreateInfo p)
        {
            return 0;
        }
    }
}