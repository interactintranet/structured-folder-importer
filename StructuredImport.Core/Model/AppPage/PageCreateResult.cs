﻿using System;

namespace StructuredImport.Core.Model.AppPage
{
    internal class PageCreateResult : PageOperationResult
    {
       public bool PageWasCreated { get; set; }

    }

    internal class PageUpdateResult : PageOperationResult
    {
        public bool PageWasUpdated { get; set; }
    }

    internal class PageOperationResult
    {
        public int PageId { get; set; }
        public string FailMessage { get; set; }
        public TimeSpan ProcessTime { get; set; }
    }
}