﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructuredImport.Core.Model.Database
{
    internal class FolderInfo
    {
        public int SectionId { get; set; }
        public int CategoryId { get; set; }
        public string FolderFullPath { get; set; }
        public string FolderName { get; set; }
        public int FolderId { get; set; }
        //public int? ParentId { get; set; }
    }
}
