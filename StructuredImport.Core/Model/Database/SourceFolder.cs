﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructuredImport.Core.Model.Database
{
    internal class SourceFolder
    {
        public int SourceFolderId { get; set; }
        public string FolderName { get; set; }
        public string RelativePath { get; set; }    
    }
}
