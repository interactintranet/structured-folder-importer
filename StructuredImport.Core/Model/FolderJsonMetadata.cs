﻿
public class FolderJsonMetadata
{
    public int sectionId { get; set; }
    public int categoryId { get; set; }
    public string defaultPageType { get; set; }
    public string[] buildPageTypes { get; set; }
    public string[] uploadPageTypes { get; set; }

    public string fileAlreadyExists { get; set; }
}

