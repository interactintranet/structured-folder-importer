﻿using System;
using StructuredImport.Core.Model.InteractPage.PageDownload;

namespace StructuredImport.Core.Model.InteractPage
{
    public class PageUpload
    {
        public Transition Transition { get; set; }
        public Page Page { get; set; }

        public PageUpload()
        {
            Transition = new Transition();
            Page = new Page();
        }
    }

    public class Transition
    {
        public string State { get; set; }
        public string Message { get; set; }
    }

    public class Page
    {
        public Page()
        {
            Features = new Features();
            Content = new Content();
            PageAttachments  = new PageAttachments();
        }

        public string ContentType { get; set; }
        public int[] TopSectionIds { get; set; }
        public int AuthorId { get; set; }
        public int? PublishAsId { get; set; }
        public int AssetId { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public Content Content { get; set; }
        public int[] CategoryIds { get; set; }
        public Features Features { get; set; }
        public DateTime PubStartDate { get; set; }
        public DateTime PubEndDate { get; set; }
        public DateTime ReviewDate { get; set; }
        public int[] TagIds { get; set; }
        public string[] Keywords { get; set; }
        public PageAttachments PageAttachments { get; set; }
        public TranslationsItem Translations { get; set; }
    }

    public class Content
    {
        public string Html { get; set; }
        public string Link { get; set; }
        public Upload Upload { get; set; }

    }

    public class Features
    {
        public Features()
        {
            Recommends = new Recommends();
        }

        public bool DefaultToFullWidth { get; set; }
        public bool AllowComments { get; set; }
        public bool IsKeyPage { get; set; }
        public Recommends Recommends { get; set; }
    }

    public class Recommends
    {
        public bool Show { get; set; }
        public int MaxContentAge { get; set; }
    }

    public class Upload
    {
        public string Title { get; set; }
        public string Address { get; set; }
        public object Target { get; set; }
        public bool GoStraightToTarget { get; set; }
        public string FileGuid { get; set; }
        public int FileSize { get; set; }
        public string Extension { get; set; }
        public bool DisableDownload { get; set; }
    }

    public class PageAttachments
    {
        public PageAttachmentFeature[] Files { get; set; }
    }
    public class PageAttachmentFeature
    {
        public string Title { get; set; }
        public string Address { get; set; }
        private string _target;
        public string Target
        {
            get => _target;
            set => _target = value.ToLower();
        }
        public bool GoStraightToTarget { get; set; }
        public string FileGuid { get; set; }
        public long FileSize { get; set; }
        public string Extension { get; set; }
        public bool DisableDownload { get; set; }
    }
}


