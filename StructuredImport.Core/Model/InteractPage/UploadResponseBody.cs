﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructuredImport.Core.Model.InteractPage
{

    public class UploadResponseBody
    {
        public bool Succeeded { get; set; }
        public object ErrorMessage { get; set; }
        public int AssetId { get; set; }
        public Fileinfo FileInfo { get; set; }
        public object AssetGuid { get; set; }
    }

    public class Fileinfo
    {
        public object Filename { get; set; }
        public object InputStream { get; set; }
        public int PartIndex { get; set; }
        public int TotalParts { get; set; }
        public object OriginalFilename { get; set; }
        public object FileUuid { get; set; }
        public int FileSize { get; set; }
        public FileUploadAttachment FileUploadAttachment { get; set; }
    }

    public class FileUploadAttachment
    {
        public int AttachmentId { get; set; }
        public string FileGuid { get; set; }
        public string Path { get; set; }
        public string Title { get; set; }
        public int Size { get; set; }
        public string Extension { get; set; }
        public int ObjectId { get; set; }
        public int ObjectTypeId { get; set; }
    }

}
