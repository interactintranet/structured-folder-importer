﻿using System;

namespace StructuredImport.Core.Model.InteractPage.PageDownload
{

    public class PageDownload
    {
        public int Id { get; set; }
        public int VersionId { get; set; }
        public int VersionNumber { get; set; }
        public Author Author { get; set; }
        public Publishedas PublishedAs { get; set; }
        public object CheckedOutBy { get; set; }
        public IdValuePair[] Tags { get; set; }
        public IdValuePair[] Keywords { get; set; }
        public Category[] Categories { get; set; }
        public PageAttachments PageAttachments { get; set; }
        public bool IsActive { get; set; }
        public bool IsArchived { get; set; }
        public bool AutoLockPage { get; set; }
        public int[] TopSectionIds { get; set; }
        public int AssetId { get; set; }
        public string ContentType { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public Content Content { get; set; }
        public Features Features { get; set; }
        public Audience Audience { get; set; }
        public DateTime PubStartDate { get; set; }
        public DateTime PubEndDate { get; set; }
        public DateTime ReviewDate { get; set; }
        public string JsonData { get; set; }
        public int? ConfidentialityId { get; set; }
        public int? ClassificationId { get; set; }
        public object[] BestBets { get; set; }
        public TranslationsItem Translations { get; set; }

        public Page MapToPage(PageDownload pageDownload)
        {
            throw new NotImplementedException();
        }
    }

    public class Author
    {
        public int Id { get; set; }
        public int AssetId { get; set; }
        public string Name { get; set; }
    }

    public class Publishedas
    {
        public int Id { get; set; }
        public int AssetId { get; set; }
        public string Name { get; set; }
    }

    //public class Pageattachments
    //{
    //    public object[] Links { get; set; }
    //    public object[] Files { get; set; }
    //}

    public class Content
    {
        public string Html { get; set; }
        public Link Link { get; set; }
        public Upload Upload { get; set; }
    }

    public class Upload
    {
        public string Title { get; set; }
        public string Address { get; set; }
        public string Target { get; set; }
        public bool GoStraightToTarget { get; set; }
        public string FileGuid { get; set; }
        public int FileSize { get; set; }
        public string Extension { get; set; }
        public bool DisableDownload { get; set; }
    }

    public class Features
    {
        public bool DefaultToFullWidth { get; set; }
        public bool AllowComments { get; set; }
        public bool IsKeyPage { get; set; }
        public Recommends Recommends { get; set; }
        public bool? IsMandatoryRead { get; set; }
    }

    public class Recommends
    {
        public bool Show { get; set; }
        public int? MaxContentAge { get; set; }
    }

    public class Audience
    {
        public object[] RelatedContent { get; set; }
        public object[] NotificationRecipients { get; set; }
    }

    public class IdValuePair
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }

    public class Category
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }

    public class Link
    {
        public string Title { get; set; }
        public string Address { get; set; }
        public string Target { get; set; }
        public bool GoStraightToTarget { get; set; }
        public string FileGuid { get; set; }
        public int FileSize { get; set; }
        public string Extension { get; set; }
        public bool DisableDownload { get; set; }
    }

    public class TranslationsItem
    {
        public int originalLanguageId { get; set; }

        public Translation[] translations { get; set; }
    }

    public class Translation
    {
        public int languageId { get; set; }
        public string title { get; set; }
        public string summary { get; set; }
        public string body { get; set; }
    }
}
