﻿namespace StructuredImport.Core.Model.InteractPage
{
    internal class InteractPageApiResponse
    {
        public int ContentId { get; set; }
        public string VersionId { get; set; }   //poss nullable int really
    }
}