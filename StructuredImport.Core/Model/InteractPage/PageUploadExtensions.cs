﻿using System;
using System.IO;
using StructuredImport.Core.Model.AppPage;

namespace StructuredImport.Core.Model.InteractPage
{
    internal static class PageUploadExtensions
    {

        public static Upload MapToUpload(this PageAttachmentFeature pageAttachmentFeature)
        {
            return new Upload
            {
                Address = pageAttachmentFeature.Address,
                DisableDownload = pageAttachmentFeature.DisableDownload,
                Extension = pageAttachmentFeature.Extension,
                FileGuid = pageAttachmentFeature.FileGuid,
                GoStraightToTarget = pageAttachmentFeature.GoStraightToTarget,
                Target = pageAttachmentFeature.Target,
                Title = pageAttachmentFeature.Title,
                FileSize = Convert.ToInt32(pageAttachmentFeature.FileSize)
            };
        }

        public static void PrepareAttachmentMetadata(this PageUpload page, FileUploadAttachment uploadAttachment, PageCreateInfo buildInfo)
        {
            page.Page.Content.Upload = new Upload
            {
                Address = uploadAttachment.Path,
                Extension = Path.GetExtension(buildInfo.FileLocation),
                FileGuid = uploadAttachment.FileGuid,
                DisableDownload = buildInfo.DisableDownload,
                Target = buildInfo.Target,
                FileSize = uploadAttachment.Size,
                GoStraightToTarget = buildInfo.GoStraightToTarget,
                Title = uploadAttachment.Title
            };
        }
    }
}
