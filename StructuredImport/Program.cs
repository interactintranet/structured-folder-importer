﻿using System.Threading.Tasks;

namespace StructuredImport
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var proc = new AppProcessor();
            //User LoadAllFolders to populate the database with the folder structure; then do the mapping
            //await proc.LoadAllFolders();
            //Once some or all the folders are mapped, use RunImportProcess to do the import
            await proc.RunImportProcess();
        }
    }
}
