﻿using System.Threading.Tasks;
using StructuredImport.Analyse;
using StructuredImport.Core.App;
using StructuredImport.Core.Model;

namespace StructuredImport
{
    internal class AppProcessor
    {
        public async Task RunImportProcess()
        {
            //var config = LoadAppConfig();
            //var processor = new ImportProcessorDatabaseDriven(config);
            //var r = await processor.RunProcess();

            var config = LoadAppConfigNyl();
            var dl = new DocumentMover(config);
            await dl.RunProcess(dryRun:false);
        }

        private AppConfig LoadAppConfigTest()
        {
            return new AppConfig
            {
                InteractApiUrl = "https://eu-lb-api-01.interactgo.com",
                InteractUserName = "richard.garner@interact-intranet.com",
                InteractPassword = "Airport5",
                InteractTenantId = "73779690-30e4-41b3-971e-68699ad30a66",
                MetadataDatabaseConnectionString = "Server=.;Database=StructuredImport_Test;Trusted_Connection=true",
                ExistenceCheckByMetadata = true,
                DefaultAuthorId = 1276,
                DefaultAssetId = 251367,
                InteractServerResourcesFolder = @"C:\Work\resources-dump",
                LogToDatabase = false
            };
        }

        public async Task LoadAllFolders()
        {
            var config = LoadAppConfigNyl();
            var analyser = new FolderAnalyser(config);
            //await analyser.LoadFolderStructureFrom(@"C:\develop\StructImporter\SampleStruct1");
            await analyser.LoadFolderStructureFrom(@"C:\Users\Richard Garner\OneDrive - Interact\Documents - Kerry McIlhaney's files");
        }
        private AppConfig LoadAppConfigNyl()
        {
            return new AppConfig
            {
                InteractApiUrl = "https://eu-lb-api-01.interactgo.com",
                InteractUserName = "richard.garner@interact-intranet.com",
                InteractPassword = "Airport5",
                InteractTenantId = "73779690-30e4-41b3-971e-68699ad30a66",
                MetadataDatabaseConnectionString = "Server=.;Database=NylAgency;Trusted_Connection=true",
                ExistenceCheckByMetadata = true,
                DefaultAuthorId = 1276,
                DefaultAssetId = 251367,
                InteractServerResourcesFolder = @"C:\Work\resources-dump",
                LogToDatabase = false
            };
        }
    }
}